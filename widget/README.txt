[ How to COPY one app's code to another one ]

-- config.xml --
widget id ( ApiCloud APP id )
ajpush app_key ( Jiguang Push Service )
umengAnalytics ios_app_channel ( mark out iOS version market name )
umengAnalytics android_app_channel ( mark out Android version market name )

-- script/skin.js --
mallSubCategory array ( App shows which categories )
APICLOUD_APP_ID ( ApiCloud APP id )
Set some default values:
  appSupportLanguage
  defaultMarketCode
  defaultCurrencyCode
  defaultLanguage

-- image/boot/1920/boot-image.jpg --
-- image/boot/1920/boot-image-local.jpg --
Need to change App boot images

-- html/account/abount-ourmall-sub.html ( or other text content files ) --
change Kazakhstan( or such like this ) to Iran ( or such like this )

-- html/cart/order-confirm-sub.html --
Need to confirm which Payment Method is supported.

-- On ApiCloud website --
Choose all modules on ApiCloud website (Reference Kazakhstan App module list)

[ How to compile iOS and Android version app ]

-- iOS version --
1) modify config.xml, userAgent value use widget://res/ua-ios.txt
2) commit config.xml to SVN, open ApiCloud website and compile iOS app.
-- Android version --
1) modify config.xml, userAgent value use widget://res/ua-android.txt
2) commit config.xml to SVN, open ApiCloud website and compile Android app.

