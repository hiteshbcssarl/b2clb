var mallSubCategory = '{"ErrorCode":"9999","Message":"SUCCESS","Data":[\
                        {"id":1,"name":"Women’s Clothing"},\
                        {"id":2,"name":"Men’s Clothing"},\
                        {"id":3,"name":"Phones & Accessories"},\
                        {"id":4,"name":"Computer & Office"},\
                        {"id":5,"name":"Consumer Electronics"},\
                        {"id":6,"name":"Jewelry & Watches"},\
                        {"id":7,"name":"Home & Garden"},\
                        {"id":8,"name":"Bags & Shoes"},\
                        {"id":9,"name":"Toys, Kids & Baby"},\
                        {"id":10,"name":"Sports & Outdoors"},\
                        {"id":11,"name":"Health & Beauty"},\
                        {"id":12,"name":"Automobiles & Motorcycles"},\
                        {"id":13,"name":"Home Improvement"},\
                        {"id":9999,"name":"Recently Viewed"}]}';

var mainMenu = [
    { id:'AppMenuHome', name: 'Home', icon: 'icon0-zoo-home'},
    { id:'AppMenuMall', name: 'Catalogue', icon: 'icon0-zoo-malllist'},
    { id:'AppMenuDeal', name: 'Sale', icon: 'icon0-zoo-deal'},
    { id:'AppMenuCart', name: 'Cart', icon: 'icon0-zoo-cart'},
    { id:'AppMenuAccount', name: 'Account', icon: 'icon0-zoo-account'}
];
var appSupportLanguage = [
    {key:'en', value:'English'},
    {key:'fa', value:'فارسی'},
    {key:'ar', value:'عربى'},
    {key:'ru', value:'русский'}
];
var appSupportCountry = [
    {key:'KZ', value:'Kazakistan',  currency: 'KZT'},
    {key:'IR', value:'Iran', currency: 'IRR'},
    {key:'LB', value:'Lebanon', currency: 'USD'}
];

var defaultMarketCode = 'IR';
var defaultCurrencyCode = 'IRR';
var defaultLanguage = 'fa';

//沉浸式
function fixStatusBar(el){
    if(api.systemType=="ios"){
        if(parseInt(api.systemVersion,10)>=7){
            el.style.paddingTop = '20px';
            el.style.backgroundColor = '#fec107';
        }else{
            el.style.paddingTop = '0px';
        }
        api.setStatusBarStyle({
            style: 'light',
            color:'#ffffff'
        });
    }else{
        var statusBar = api.require('statusBar');
        //同步返回结果：
        var statusBarHeight  = statusBar.getStatusBarHeight();
        if(parseFloat(api.systemVersion)>=4.4 && parseFloat(api.systemVersion)<=5.0){
            el.style.paddingTop = statusBarHeight+'px';
            el.style.backgroundColor = '#fec107';
        }else if(parseFloat(api.systemVersion)>5.0){
            el.style.paddingTop = statusBarHeight+'px';
            el.style.backgroundColor = '#fec107';
        }else{
            el.style.paddingTop = '0px';
        }
    }
}

localStorage.setItem('APICLOUD_APP_ID', 'A6051410901968'); // APICLOUD APP ID
