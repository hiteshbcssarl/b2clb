/*! Lazy Load 1.9.7 - MIT license - Copyright 2010-2015 Mika Tuupola */
!function(a,b,c,d){var e=a(b);a.fn.lazyload=function(f){function g(){var b=0;i.each(function(){var c=a(this);if(!j.skip_invisible||c.is(":visible"))if(a.abovethetop(this,j)||a.leftofbegin(this,j));else if(a.belowthefold(this,j)||a.rightoffold(this,j)){if(++b>j.failure_limit)return!1}else c.trigger("appear"),b=0})}var h,i=this,j={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!1,appear:null,load:null,placeholder:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"};return f&&(d!==f.failurelimit&&(f.failure_limit=f.failurelimit,delete f.failurelimit),d!==f.effectspeed&&(f.effect_speed=f.effectspeed,delete f.effectspeed),a.extend(j,f)),h=j.container===d||j.container===b?e:a(j.container),0===j.event.indexOf("scroll")&&h.bind(j.event,function(){return g()}),this.each(function(){var b=this,c=a(b);b.loaded=!1,(c.attr("src")===d||c.attr("src")===!1)&&c.is("img")&&c.attr("src",j.placeholder),c.one("appear",function(){if(!this.loaded){if(j.appear){var d=i.length;j.appear.call(b,d,j)}a("<img />").bind("load",function(){var d=c.attr("data-"+j.data_attribute);c.hide(),c.is("img")?c.attr("src",d):c.css("background-image","url('"+d+"')"),c[j.effect](j.effect_speed),b.loaded=!0;var e=a.grep(i,function(a){return!a.loaded});if(i=a(e),j.load){var f=i.length;j.load.call(b,f,j)}}).attr("src",c.attr("data-"+j.data_attribute))}}),0!==j.event.indexOf("scroll")&&c.bind(j.event,function(){b.loaded||c.trigger("appear")})}),e.bind("resize",function(){g()}),/(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion)&&e.bind("pageshow",function(b){b.originalEvent&&b.originalEvent.persisted&&i.each(function(){a(this).trigger("appear")})}),a(c).ready(function(){g()}),this},a.belowthefold=function(c,f){var g;return g=f.container===d||f.container===b?(b.innerHeight?b.innerHeight:e.height())+e.scrollTop():a(f.container).offset().top+a(f.container).height(),g<=a(c).offset().top-f.threshold},a.rightoffold=function(c,f){var g;return g=f.container===d||f.container===b?e.width()+e.scrollLeft():a(f.container).offset().left+a(f.container).width(),g<=a(c).offset().left-f.threshold},a.abovethetop=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollTop():a(f.container).offset().top,g>=a(c).offset().top+f.threshold+a(c).height()},a.leftofbegin=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollLeft():a(f.container).offset().left,g>=a(c).offset().left+f.threshold+a(c).width()},a.inviewport=function(b,c){return!(a.rightoffold(b,c)||a.leftofbegin(b,c)||a.belowthefold(b,c)||a.abovethetop(b,c))},a.extend(a.expr[":"],{"below-the-fold":function(b){return a.belowthefold(b,{threshold:0})},"above-the-top":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-screen":function(b){return a.rightoffold(b,{threshold:0})},"left-of-screen":function(b){return!a.rightoffold(b,{threshold:0})},"in-viewport":function(b){return a.inviewport(b,{threshold:0})},"above-the-fold":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-fold":function(b){return a.rightoffold(b,{threshold:0})},"left-of-fold":function(b){return!a.rightoffold(b,{threshold:0})}})}(jQuery,window,document);

//lazyload
function imgLoad(){
	var option = {
		threshold: 100,
		load:function(){
			var obj = $(this);
			obj.attr("data-lazyload",false);
			if(obj.parent().parent().hasClass('mui-grid-9')){
				var _img = new Image();
				_img.src = obj.attr("data-original");
				_img.onload = function(){
					var imgW = _img.width,
						imgH = _img.height;
					if(imgW>=imgH){
						obj.css("background-size","auto 100%");
					}else{
						obj.css({"background-size":"100% auto"});
					}
				}

			}
			if(obj.parent().parent().parent('#divReviewImage').length>0){
				imgWH(this);
			}
			if(typeof(obj.attr('data-autowh'))!='undefined'){
				var _img = new Image();
				_img.src = obj.attr("data-original");
				_img.onload = function(){
					var imgW = _img.width,
						imgH = _img.height;
					if(imgW>=imgH){
						obj.css("background-size","auto 100%");
					}else{
						obj.css({"background-size":"100% auto"});
					}
				}
			}

		}
	}
	jQuery("[data-lazyload=true]").lazyload(option);
}

function getCId(){
	var localUinfo = localStorage.getItem("OURMALL_USERINFO");
	localUinfo = localUinfo ? JSON.parse(localUinfo) : "";
	if(localUinfo && localUinfo.customerId){
		return localUinfo.customerId;
	}else{
		return "";
	}
}
function getUserInfo(callback, force) {
	force = force ? force : false;
	var userDataString = '';
	var userData = null;
	var nowTimestamp = Date.parse(new Date()) / 1000;
	var lastCheckTimestamp = localStorage.getItem("LAST_CHECK_TIMESTAMP") ? localStorage.getItem("LAST_CHECK_TIMESTAMP") : 0;
	var validLoginDay = 1;
	if(userDataString = localStorage.getItem("OURMALL_USERINFO")) {
		userData = JSON.parse(userDataString);
		if(lastCheckTimestamp == 0 && userData.timeLogin) {
			lastCheckTimestamp = Date.parse(new Date(userData.timeLogin.replace(/-/g, "/"))) / 1000;
		}
	}
	if(nowTimestamp - lastCheckTimestamp >= 86400 * validLoginDay) {
		//登录超过 validLoginDay 天，主动从app退出登录，重新检查登录信息
		userData = null;
		localStorage.removeItem('OURMALL_USERINFO');
	}
	if(userData && (!force)) {
		//如果有本地登录信息，直接返回
		if(typeof(callback) == 'string'){
			if(callback) {
				eval(callback);
			} else {
				return true;
			}
		} else {
			callback(userData);
		}
	} else {
		localStorage.setItem("LAST_CHECK_TIMESTAMP", nowTimestamp);
		var UILoading = api.require('UILoading');
		var loadingId;
		if(callback) {
			UILoading.flower({
			    center: {
			        x: api.winWidth / 2.0,
					y: api.winHeight / 2.0
			    },
			    size: 40,
			    fixed: true
			}, function(ret) {
			    loadingId = ret;
			});
		}
		var type = 0;
		if(api.systemType == 'ios') {
			type = 2;
		} else {
			type = 1;
		}
		$.mamall_request('member.getmemberbyuids', {
			mPlatform: type,
			jpushCode: localStorage.getItem("OURMALL_RGID"),
			OM_DEVICE_UUID: api.deviceId,
			H5_API_REQUEST_CACHE_SET: 2
		}, function(r) {

			if(loadingId) {
				UILoading.closeFlower(loadingId);
			}
			if('9999' == r.ErrorCode) {
				var userData = r.Data.Customer;
				if(userData.customerId){
					userData.channeId = userData.channeId ? userData.channeId : 0;
					userData.portrait = userData.portrait ? userData.portrait : '../../image/avatar/' + (parseInt(userData.customerId)%10) + '.png';
					localStorage.setItem("OURMALL_USERINFO",JSON.stringify(userData));
					if(typeof(callback) == 'string'){
						if(callback) {
							eval(callback);
						} else {
							return true;
						}
					} else {
						callback(userData);
					}
				}else{
					if(callback != '') {
//						if (api.winName == 'main') {
//							api.execScript({
//							    name: 'main',
//							    script: 'changeFrame(document.getElementById("AppMenuAccount"), 3, true);'
//							});
//						} else{
//							api.execScript({
//							    name: 'main',
//							    script: 'changeFrame(document.getElementById("AppMenuAccount"), 3, true);'
//							});
//							api.closeWin({name:api.winName});
//							api.closeFrame({name:api.frameName});
//						}
						$api.openWin({
							name: "login",
							url: "widget://html/account/login.html",
							animation: {
								type: "movein",
								subType: "from_bottom"
							},
							reload: true
						});
					} else {
						return false;
					}
				}
			}
		},"",api);
	}
	return false;
}

function goCart(){
	api.execScript({
	    name: 'main',
	    script: 'changeFrame(document.getElementById("AppMenuCart"), 2, true);'
	});
	$api.openWin({
    	name: 'main',
    	slidBackEnabled:false,
    	animation:{
	    	type:'none'
	    }
	});
	setTimeout(function(){ api.closeWin({}) }, 200);
}
function scrollToTop(){
	$('html, body').animate({scrollTop:0}, 200);
}
function formatNumber(num, fixed) {
	// 弃用，使用roundFixed 代替
	num = num.toString().replace(/\$|\,/g,'');
	fixed = fixed ? fixed : 2;
	num = String(num).split(".");
	num[0] = (num[0] || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
	if(num[1]){
		num = num[0]+'.'+num[1].substring(0, fixed);
	}else{
		num = num[0]
	}
	return num;
}
function roundFixed(num, fixed) {
	if(isNaN(num)) return '';
	fixed = fixed ? fixed : 2;
    var pos = num.toString().indexOf('.'),
        decimal_places = num.toString().length - pos - 1,
        _int = num * Math.pow(10, decimal_places),
        divisor_1 = Math.pow(10, decimal_places - fixed),
        divisor_2 = Math.pow(10, fixed);
    return Math.round(_int / divisor_1) / divisor_2;
}
function formatNumberAsForeigner(num, point) {
	point = point ? point : 0;
	var str = parseFloat(num) + '';
	var len = str.length;
	var newNumberStr = 0;
	if(len <= 3) {
	    newNumberStr = str;
	} else if (len <= 6) {
	    point = str.substr(len-3, point);
	    newNumberStr = str.substr(0, len-3) + (point ? '.' + point : '') + 'K';
	} else if (len <= 9) {
	    point = str.substr(len-6, point);
	    newNumberStr = str.substr(0, len-6) + (point ? '.' + point : '') + 'M';
	} else if (len <= 12) {
	    point = str.substr(len-9, point);
	    newNumberStr = str.substr(0, len-9) + (point ? '.' + point : '') + 'G';
	} else{
	    point = str.substr(len-12, point);
	    newNumberStr = str.substr(0, len-12) + (point ? '.' + point : '') + 'T';
	}
	return newNumberStr;
}

function cartReady(){
	$.mamall_request('b2c.cart.check', {
		OM_DEVICE_UUID: api.deviceId
	}, function(r) {
		if('9999' == r.ErrorCode) {
			if(r.Data){
				$api.setStorage("cart",'true');
			}else{
				$api.setStorage("cart","false");
			}
			setTimeout(function(){
				cartActive($api.getStorage("cart"));
			},100);
		}
	},"",api);

	api.addEventListener({
	    name: 'cartListener'
	}, function(ret, err) {
		cartActive(ret.value.Key);
	});

}
function cartActive(on){
	if(on==="true"){
		$(".nui-header-item .icon-gouwuche").addClass("active");
		$(".bottom-button-group [data-type='cart']").addClass("active");
	}else{
		$(".nui-header-item .icon-gouwuche").removeClass("active");
		$(".bottom-button-group [data-type='cart']").removeClass("active");
	}
}
function gotoZoodelWeb(url, name) {
	name = name ? name : '';
	if(sysType == 'ios') {
		var type = 2;
	} else {
		var type = 1;
	}
	var sysType = api.systemType;
	var jpId = localStorage.getItem("OURMALL_RGID")?localStorage.getItem("OURMALL_RGID"):"";
	if(! getCId()) {
		$api.setStorage('LAST_PAGE_BEFORE_LOGIN', {type:'ZoodelWeb', name:name, url:url});
	}
	var redirectUrl = hostURL() + "?m=main&a=redirectFromApp&hideH5Menu=1&hideH5MenuFromNewApp=1&mPlatform=" + type + "&jpushCode=" + jpId + "&OM_DEVICE_UUID=" + api.deviceId;
	url = redirectUrl + '&redirectUrl=' + encodeURIComponent(url);
	$api.openWin({
		name: "ZoodelWeb_" + name.replace(/ /g, '_'),// + '_' + Math.random(),
		url: "widget://html/product/webpage.html",
		pageParam: {_url:url,name:name},//这里是要打开的网址
		animation: {
			type: "movein",
			subType: "from_right"
		},
		reload: true,
		slidBackEnabled: false
	});
}
function hostURL(){
    var host = '';
    var HTTP_HEADER = '';
    if(api.debug == true) {
        HTTP_HEADER = 'https';
		host = HTTP_HEADER + '://b2c.zoodmall.com/index.php';
    }else{
		HTTP_HEADER = 'https';
		host = HTTP_HEADER + '://b2c.zoodmall.com/index.php';
	}
    return host;
}
function openFrameReload(index,type){
	if(!type){
		type = "mall"
	}
	api.setFrameGroupIndex({
		name: type+'Group',
		index: index,
		scroll: true,
		reload:true
	});
}
//pay success
function paySuccess(type){
	api.execScript({
		name: 'my-orders',
		script:'api.closeFrameGroup({name: "my-orders-group"})'
	});
	$api.openWin({
			name:'my-orders',
			url : 'widget://html/account/my-orders.html',
				animation: {
					type: "movein",
					subType: "from_right"
				},
			reload:true,
			pageParam:{
				status:0
			}
	});
	setTimeout(function(){
		api.execScript({
			name:'my-orders',
			script:'statausBAR()'
		});
	},1200);
	setTimeout(function(){
		if(type != 'true'){
				api.execScript({
					name:'main',
					script:'changeFrame(document.getElementById("AppMenuCart"), 2, true);'
				});
		}

		setTimeout(function(){
			api.closeWin({
			    name: 'order-confirm',
			    animation:{
			    	type:'none'
			    }
			});
		},500);
		setTimeout(function(){
			api.closeWin({
			    name: 'order-detail',
			    animation:{
			    	type:'none'
			    }
			});
		},700);

	},700);
}
function startPaypost(customerId,paymentData){
	$api.setStorage('REFRESH_CART_PAGE', true);
	var jsonOrderCodeData = new Object;
	var paymentData2 = new Object;
	for(var i in paymentData){
		jsonOrderCodeData[i] = new Object;
	}
	for (var j in jsonOrderCodeData){
		jsonOrderCodeData[j].goodsAmount = paymentData[j].goodsAmount
		jsonOrderCodeData[j].shippingAmount = paymentData[j].shippingAmount
		jsonOrderCodeData[j].couponAmount = paymentData[j].couponAmount
	}
	paymentData2.paymentType = 5; //PayPost
	paymentData2.customerId = customerId;
	paymentData2.jsonOrderCode = JSON.stringify(jsonOrderCodeData);

	$.mamall_request('order.getpayurl',paymentData2, function(r) {
		if(r.ErrorCode == '9999') {
			var pay2Url = '';
			pay2Url = r.Data.payUrl;
			$api.openWin({
				name: "paypost",
				url: "widget://html/product/webpage.html",
				pageParam: {_url:pay2Url,name:$.i18n.prop("Payment")},//这里是要打开的网址
				animation: {
					type: "movein",
					subType: "from_right"
				},
				reload: true,
				slidBackEnabled: false
			});
		} else {
			nui.toast(r.Message);
			api.sendEvent({
			    name: 'cancelPay'
			});
		}
	},undefined,api);
}
//打开二维码
function openQrCodeByStr(str){
	api.openFrame({
		name: 'str-to-qrcode',
		url: 'widget://html/main/str-to-qrcode.html',
		bounces: false,
		bgColor: 'rgba(0,0,0,.7)',
		pageParam: {
			str:str,
		},
		vScrollBarEnabled: false,
		hScrollBarEnabled: false,
		reload: true
	});
}
function startCouponPop(customerId){
	var _switch = true,
		status;
	var popOption = new Object;
	popOption.customerId = customerId;
	popOption.source = 1;//source = 1 新人礼包
	// //app是否领过优惠券
	var couponStatus = $api.getStorage('GET_FIRST_COUPON_STATUS') == 'true' ? true : false;
	if(couponStatus) return;

	$.mamall_request('coupon.hasget', popOption, function(r) {
    	if (r.ErrorCode == '9999'){
    		_switch = r.Data.hasCoupon;
    		status = r.Data.status;
    		faceValue = r.Data.faceValue;
    		if(faceValue > 0) {
    			//有可领的优惠券，弹出提示层
    			if(!_switch){
	    			api.openFrame({
						name: 'pop-coupon-mask',
						url: 'widget://html/pop/pop-coupon-mask.html',
						bounces: false,
						bgColor: 'rgba(0,0,0,.7)',
						vScrollBarEnabled: false,
						hScrollBarEnabled: false,
						pageParam: {
							first: status == 'new' ? '1':'',
							faceValue: faceValue
						}
					});
	    		} else {
	    			$api.setStorage('GET_FIRST_COUPON_STATUS', 'true');
	    		}
    		}
    	}
    }, undefined, api);
}
function closeCouponPop() {
	api.execScript({
		name:'main',
		frameName:'pop-coupon-mask',
		script:"api.closeFrame({name: 'pop-coupon-mask'});"
	});
	api.execScript({
		name:'main',
		frameName:'pop-fans-coupon-mask',
		script:"api.closeFrame({name: 'pop-fans-coupon-mask'});"
	});
	api.execScript({
		name:'pro-detail-platform',
		frameName:'pop-fans-coupon-mask',
		script:"api.closeFrame({name: 'pop-fans-coupon-mask'});"
	});
	$api.rmStorage('SHOW_POP_FRAME_LOCK');
}
function callAppPage(appParam) {
	if(appParam) {
		appParam = JSON.parse(appParam);
		if(appParam && appParam.type) {
			switch(appParam.type) {
				case 'shop':
					$api.openWin({
				        name: 'shop',
				        url: "widget://html/product/shop.html",
				        pageParam: {
				        	_id: appParam.id
				        },
				        animation: {
				            type: "movein",
				            subType: "from_right"
				        },
				        reload: true
				    });
					break;
				case 'product': // 商品详情
					var isOurmall = appParam.isOurmall;
					var pid = appParam.pid;
					var productId = appParam.productId;
					var sponsorShopId = appParam.sponsorShopId;
					if(isOurmall && ((pid) || (productId && sponsorShopId))) {
						openProDetail(isOurmall, pid, productId, sponsorShopId);
					}
					break;

				case 'mall'://mall菜单
					if($api.getStorage('SHOW_POP_FRAME_LOCK') == 'true') return;
					api.execScript({
						name:'main',
						script:'changeFrame(document.getElementById("AppMenuMall"), 1, true)'
					});
					break;

				case 'account'://account菜单
					if($api.getStorage('SHOW_POP_FRAME_LOCK') == 'true') return;
					api.execScript({
						name:'main',
						script:'changeFrame(document.getElementById("AppMenuAccount"), 3, true)'
					});
					break;
				case 'cart'://cart列表菜单
					if($api.getStorage('SHOW_POP_FRAME_LOCK') == 'true') return;
					api.execScript({
						name:'main',
						script:'changeFrame(document.getElementById("AppMenuCart"), 2, true)'
					});
					break;
				case 'openUrl'://webview打开链接
					gotoZoodelWeb(appParam.url, 'Zoodel');
					break;
				case 'newCoupon':
					UILoading = api.require('UILoading');
					UILoading.flower({
				        center: {
				            x: api.winWidth / 2.0,
				            y: api.winHeight / 2.0
				        },
				        size: 40,
				        fixed: true
				    }, function(ret) {
						//startCouponPop(getCId());
				        setTimeout(function(){ UILoading.closeFlower(ret); }, 1000);
				    });
					break;
	    	}
	    }
	}
}

//打开付款遮罩
function payMask(type,msg){
	if(!type){
		$("body").append('<div class="pay-wait-mask">\
			<div class="pay-wait-inner">\
				<p>' + $.i18n.prop("Payment in processing") + '…</p>\
				<div style="position: relative;">\
					<div class="loader"></div>\
					<div class="sa-icon sa-success animate" style="display: none;">\
				      <span class="sa-line sa-tip "></span>\
				      <span class="sa-line sa-long "></span>\
				    </div>\
				    <div class="sa-icon sa-error animateErrorIcon" style="display: none;">\
				      <span class="sa-x-mark animateXMark">\
				        <span class="sa-line sa-left"></span>\
				        <span class="sa-line sa-right"></span>\
				      </span>\
				    </div>\
				</div>\
			</div>\
		</div>')
		$(".pay-wait-mask").show();
		return;
	}
	switch(type){
		case 'success':
			$(".pay-wait-mask").addClass('active');
			$(".pay-wait-inner p").text("Pay succeed");
			$(".sa-line").css("opacity",0);
			setTimeout(function(){
				$(".pay-wait-mask .loader").css("border-bottom-color","transparent");
			},500);
			setTimeout(function(){
				$(".pay-wait-mask .loader").css("border-left-color","transparent");
			},600);
			setTimeout(function(){
				$(".pay-wait-mask .loader").css("border-top-color","transparent");
				$(".sa-success").show();
				$(".sa-tip").addClass("animateSuccessTip").css("opacity",1);
			},700);
			setTimeout(function(){
				$(".sa-long").addClass("animateSuccessLong").css("opacity",1);
			},1450);
			setTimeout(function(){
				$(".pay-wait-mask .loader").css({
					'border-bottom-color':"#A5DC86",
					'border-left-color':"#A5DC86",
					'border-top-color':"#A5DC86",
					'border-right-color':"#A5DC86",
					'opacity':".3"
				});
			},2300)
		break;
		case 'failed':
			$(".pay-wait-mask").addClass('active');
			$(".pay-wait-inner p").text(msg ? msg : $.i18n.prop("Payment canceled")).css("color",'#E62117');
			setTimeout(function(){
				$(".pay-wait-mask .loader").css("border-bottom-color","transparent");
			},500);
			setTimeout(function(){
				$(".pay-wait-mask .loader").css("border-left-color","transparent");
			},600);
			setTimeout(function(){
				$(".pay-wait-mask .loader").css("opacity",0);
			},700);
			setTimeout(function(){
				$(".sa-error").show();
				$(".sa-x-mark").addClass("animateXMark");
			},1200);

		break;
	}
}
function getLocalTimeFromDatetime(datetime, type) {
	var timestamp = (new Date(datetime.replace(/-/g, '\/'))).getTime()/1000;
	switch(type) {
		case 'en':
			return formatEnTime(timestamp);
			break;
		default:
			return getLocalTime(timestamp);
			break;
	}
	return '';

}
function getLocalTime(nS) {
    return  new Date(parseInt(nS) * 1000).Format("MM-dd-yyyy hh:mm");
}
function formatEnTime(nS){
	var time = new Date(parseInt(nS) * 1000).Format("MM-dd-yyyy").split('-');
	var MM = time.shift();
	switch (MM)
	{
		case '01':
			MM = 'Jan'
		break;
		case '02':
			MM = 'Feb'
		break;
		case '03':
			MM = 'Mar'
		break;
		case '04':
			MM = 'Apr'
		break;
		case '05':
			MM = 'May'
		break;
		case '06':
			MM = 'Jun'
		break;
		case '07':
			MM = 'Jul'
		break;
		case '08':
			MM = 'Aug'
		break;
		case '09':
			MM = 'Sep'
		break;
		case '10':
			MM = 'Oct'
		break;
		case '11':
			MM = 'Nov'
		break;
		case '12':
			MM = 'Dec'
		break;
	}
	return time[0]+' ' + MM +' '+time[1]
}
function formatEnTimeHM(nS){
	var time = new Date(parseInt(nS) * 1000).Format("MM-dd-yyyy hh:mm").split('-');
	var MM = time.shift();
	switch (MM)
	{
		case '01':
			MM = 'Jan'
		break;
		case '02':
			MM = 'Feb'
		break;
		case '03':
			MM = 'Mar'
		break;
		case '04':
			MM = 'Apr'
		break;
		case '05':
			MM = 'May'
		break;
		case '06':
			MM = 'Jun'
		break;
		case '07':
			MM = 'Jul'
		break;
		case '08':
			MM = 'Aug'
		break;
		case '09':
			MM = 'Sep'
		break;
		case '10':
			MM = 'Oct'
		break;
		case '11':
			MM = 'Nov'
		break;
		case '12':
			MM = 'Dec'
		break;
	}
	return time[0]+' ' + MM +' '+time[1]
}
Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };

        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
//是否空对象
function isEmptyObject(e) {
    var t;
    for (t in e)
        return !1;
    return !0
}
//数组去重
function unique(array){
	var r = [];
	for(var i = 0, l = array.length; i < l; i++) {
		for(var j = i + 1; j < l; j++)
			if (array[i] === array[j]) j = ++i;
		r.push(array[i]);
	}
	return r;
}
function saveLocalPageData(key, data, page, totalPage) {
	var saveData = {};
	var timestamp = Date.parse(new Date());
	timestamp = timestamp / 1000;
	saveData.html = data;
	saveData.timestamp = timestamp;
	saveData.page = page;
	saveData.totalPage = totalPage;
	key = 'PAGE_CACHE_DATA_' + key;
	$api.setStorage(key, saveData);
	return true;
}
function getLocalPageData(key) {
	key = 'PAGE_CACHE_DATA_' + key;
	return $api.getStorage(key);
}
function removeLocalPageData(key) {
	key = 'PAGE_CACHE_DATA_' + key;
	$api.rmStorage(key);
	return true;
}

filterBadgeActive = function(){
	switch($api.getStorage('activeMainMenu')){
		case 'mall':
			var cid = $api.getStorage('activeMallCategroy');
			var filterData = JSON.parse($api.getStorage('MallProductCategoryFilter'));
			if( !cid || cid == '9998' || cid == '9999' || cid == '9997') return;
			if(typeof filterData[cid].specificsQueryCond == 'object'){
				filterData[cid].specificsQueryCond = filterData[cid].specificsQueryCond.join(',');
				filterData[cid].specificsQueryCond = filterData[cid].specificsQueryCond.replace(/All/g,'');
				filterData[cid].specificsQueryCond = filterData[cid].specificsQueryCond.replace(/,/g,'');
			}
			if( filterData[cid].nameLike || filterData[cid].minPrice || filterData[cid].maxPrice || filterData[cid].sortBy || filterData[cid].specificsQueryCond){
				$(".header-container .right-btns .icon0-filter .badge").addClass('active');
			}else{
				$(".header-container .right-btns .icon0-filter .badge").removeClass('active');
			}
			break;
		default:
			break;
	}

}
function in_array(val, arr) {
	for(var i = 0; i < arr.length; i ++) {
		if(arr[i] == val) {
			return true;
		}
	}
	return false;
}
function shakeRotate(){
	$('.pop-switch.shake-rotate').addClass('active');
	setTimeout(function(){
			$('.pop-switch.shake-rotate').removeClass('active');
		},1000);
}
function imgWH(el){
	var Img = new Image(),
		_src = $(el).prop('src');
	Img.src = _src;
	Img.onload = function(){
		var imgW = el.width,
			imgH = el.height;
		if(imgW>=imgH){
			$(el).height(70).width('auto');
		}else{
			$(el).width(70).height('auto');
		}
	}
}
function autocompleActive(_switch){
	if(_switch){
		$('.search-comple').show();
		$('#wrap').hide();
	}else{
		$('.search-comple').hide();
		$('#wrap').show();
	}

}

function rightTopBtnStatus(){
	var menuActive = $api.getStorage('activeMainMenu'),
		mallMenuActive =  $api.getStorage('activeMallCategroy'),
		showMenuActive =  $api.getStorage('activeShowCategroy');

	$('#header .right-btns span').hide(); //reset
	if( menuActive == 'mall'){
		if(mallMenuActive == '9998' || mallMenuActive == '9997'){
			$('#header .right-btns span.icon0-search').show();
		}
		if(mallMenuActive && mallMenuActive != '9998' && mallMenuActive != '9999' && mallMenuActive != '9997'){
			$('#header .right-btns span.icon0-filter').show();
		}else if(mallMenuActive == '9999'){
			$('#header .right-btns span[title=mallDel]').show();
		}
	}else if( menuActive == 'show' ){
		if(showMenuActive && showMenuActive != '9998' && showMenuActive != '9999'){
			$('#header .right-btns span.icon0-filter').show();
		}else if(showMenuActive == '9999'){
			$('#header .right-btns span[title=showDel]').show();
		}
	}
}

function delRecentlyView(type){
	var clearCacheFunction = menuKey = '';
	switch(type) {
		case 'mall':
			if(! localStorage.getItem("RV-Key")) return;
			menuKey = 'AppMenuMall';
			clearCacheFunction = 'localStorage.removeItem("RV-Key");';
			break;
	}

	api.confirm({
		title: '',
	    msg: $.i18n.prop("Confirm to clear the view history?"),
	    buttons: [$.i18n.prop("OK"), $.i18n.prop("CANCEL")]
	}, function(ret, err) {
	    var index = ret.buttonIndex;
	    if(index==1){
	    	eval(clearCacheFunction);
	    	api.execScript({
	    		name:'main',
				script: 'goTopAndRefresh("' + menuKey + '", true);'
	    	});
	    }
	});
}

function openShareMask(_targets,_data){
	UILoading = api.require('UILoading');
	UILoading.flower({
        center: {
            x: api.winWidth / 2.0,
            y: api.winHeight / 2.0
        },
        size: 40,
        fixed: true
    }, function(ret) {
        setTimeout(function(){ UILoading.closeFlower(ret); }, 500);
    });
	api.openFrame({
		name: 'share-mask',
		url: 'widget://html/main/share-mask.html',
		pageParam: {
			_targets: _targets,
			_data: _data
		},
		bounces: false,
		bgColor: 'rgba(0,0,0,.7)',
		vScrollBarEnabled: false,
		hScrollBarEnabled: false
	});
	$api.setStorage("openShareState",true);
	api.addEventListener({
		name : 'keyback'
	}, function (ret, err) {
		if($api.getStorage("openShareState")){
			api.execScript({
	    		frameName: 'share-mask',
				script: 'closeMask()'
	    	});
		}
	});
}

//打开客服聊天
function openChatMessage(){
	var _data = new Object;
	switch(api.winName){
		case 'pro-detail-platform'://ourmall商品页面
			_data.pic =  listJson.imgUrl1;
			_data.title = listJson.name;
			_data.price = listJson.defaultPrice;
			_data.pid = api.pageParam._pid;
			_data.sid = api.pageParam._sid;
			break;
		case 'order-detail': //订单详情
			_data.pic = detailData.OrderDetail.orderPlus[0].img;
			_data.title = detailData.OrderDetail.code;
			_data.price = detailData.OrderDetail.itemTotal;
			_data.code = orderCode;
			break;
		case 'main':
			//沉浸式状态栏
			if(api.systemType=="ios"){
	            if(parseInt(api.systemVersion,10)>=7){
		               api.addEventListener({
						    name:'viewappear'
						}, function(ret, err){
							setTimeout(function(){
								api.setStatusBarStyle({
								    style: 'light',
								    color:'transparent'
								});
							},200);
						});
		           }
		    	}
			break;
		//default:

	}
	//
	$api.openWin({
	    name:'chat-message',
	    url: 'widget://html/main/chat-message.html',
	    animation:{
	    	type:"movein",
	    	subType:"from_right"
	    },
	    bgColor:'rgba(255,255,255,0)',
	    reload: true,
	    slidBackEnabled: false,
	    pageParam:{
	    	winName: api.winName,
	    	frameName: api.frameName,
	    	parentPageData: _data
	    }
	});
}

function HTMLEncode(html) {
    var temp = document.createElement("div");
    (temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html);
    var output = temp.innerHTML;
    temp = null;
    return output;
}
function startCheckPushStatus(type){
	var lastTimeCheckPush = localStorage.getItem("LAST_TIME_CHECK_PUSH");
	var d = new Date();
	var today = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
	if(lastTimeCheckPush && lastTimeCheckPush != today) {
		var ajpush = api.require('ajpush');
		if(api.systemType != 'ios') {
			//android
			ajpush.init(function(ret) {
				if(ret.status == 1){
					ajpush.isPushStopped(function(cRet) {
					    if(cRet && cRet.isStopped == true){
							checkPushStatusAlert(type);
					    }
					});
				}
			});
		} else {
			//ios
			ajpush.isPushStopped(function(cRet) {
			    if(cRet && cRet.isStopped == true){
					checkPushStatusAlert(type);
			    }
			});
		}
	}
	if(lastTimeCheckPush == null) {
		localStorage.setItem("LAST_TIME_CHECK_PUSH", today);
	}

}
function checkPushStatusAlert(type) {
	//检查各种状态下是否打开了推送开关
	var keyPlusString = type + '_VER_' + api.appVersion;
	var pushStatusKey = 'PUSH_STATUS_' + keyPlusString;
	var todayNotifyKey = 'PUSH_STATUS_TODAY_NOTIFY_' + keyPlusString;
	var alertStatus = $api.getStorage(pushStatusKey);
	var todayNotify = $api.getStorage(todayNotifyKey);
	todayNotify = todayNotify ? parseInt(todayNotify) : 0;
	var message = '';
	var title = $.i18n.prop("Enable ZoodMall push notifications");
	var timestamp = Date.parse(new Date());
	timestamp = timestamp / 1000;

	if((alertStatus != 'true') && (todayNotify + 86400 < timestamp)) {
		$api.setStorage(todayNotifyKey, timestamp);
		switch(type) {
			case 'order':
				message = $.i18n.prop("Push you the lastest order's status immediately!");
				break;
			case 'common':
			default:
				message = $.i18n.prop("Special coupons, fashion items, you wouldn't miss it!");
				break
		}
		api.confirm({
			title: title,
		    msg: message,
		    buttons: [$.i18n.prop("No, thanks."), $.i18n.prop("Sure, let me try!")]
		}, function(ret, err) {
		    if(ret && ret.buttonIndex == 2) {
		    	api.openApp({
				    iosUrl: 'app-settings:',
				    androidPkg: 'com.android.settings'
				}, function(ret, err) {

				});
		    } else {
		    	//确定不打开，当前版本不再提醒
		    	$api.setStorage(pushStatusKey, 'true');
		    }
		});
	}
}

function scrollToBottom(){
	setTimeout(function(){
		localStorage.setItem('scrollToBottomFlag','true');
	},500);
	if(localStorage.getItem('scrollToBottomFlag') == 'false') return false;
	$('html,body').animate({scrollTop: $(document).height() + $(window).height() + 1000}, 500);
	localStorage.setItem('scrollToBottomFlag','false');
}
var checkCustomerServiceTimer;

//获取未读取客服系统消息
function getCustomerService(){
	clearTimeout(checkCustomerServiceTimer);
	api.addEventListener({
	    name: 'CustomerServiceStatus'
	}, function(ret, err) {
		if(ret.value.status == true){
			$('#toChatMessage').addClass('active');
			localStorage.setItem('CustomerServiceUnreadStatus', 'true');
		}else{
			localStorage.removeItem('CustomerServiceUnreadStatus');
			$('#toChatMessage').removeClass('active');
		}
	});
	if(!$api.getStorage('chatVisit') &&  typeof $api.getStorage('OURMALL_CHATLIST') != 'object' ){
        //重来没有访问过chat加红点
	    api.sendEvent({
		    name: 'CustomerServiceStatus',
		    extra: { status: true }
		});
	}
	if(getCId()) {
		$.mamall_request('b2c.account.menunumber',{
	        OM_DEVICE_UUID:api.deviceId,
	        customerId:getCId()
	   }, function(r) {
	        if('9999' == r.ErrorCode) {
	        	if(r.Data.CustomerService && r.Data.CustomerService.cnt > 0) {
	        		//检查是否有未读客服留言
				    api.sendEvent({
					    name: 'CustomerServiceStatus',
					    extra: { status: true }
					});
	        	}
	        }
	    }, undefined, api);
	}
	//加定时器，10分钟检查一次
	checkCustomerServiceTimer = setTimeout(function() {
		getCustomerService();
	}, 600000);
}

//插入客服图标
$(function(){
	var _html = '<li class="ourmallfont icon0-cs" tapmode onclick="openChatMessage()" id="toChatMessage"></li>';
	if($('#scroll-top').length>0){
		$('.button-menu>#scroll-top').before(_html);
	}else{
		$('.button-menu').append(_html);
	}
	if(localStorage.getItem('CustomerServiceUnreadStatus') == 'true') {
		$('#toChatMessage').addClass('active');
	}
	sideCouponsStatus();
});
//商品size-guide
openSizeGuide =	function (){
	$api.openWin({
	    name: 'pro-size-guide',
	    url: 'widget://html/product/pro-size-guide.html',
	    bounces: false,
	    bgColor:'#fff',
	    reload:'true',
	    progress:{
	    	type:'page',
	    	color:'#005aa9'
	    }
	});
}

//沉浸式
function fixStatusBar(el){
    if(api.systemType=="ios"){
        if(parseInt(api.systemVersion,10)>=7){
               el.style.paddingTop = '20px';
               el.style.backgroundColor = '#fec107';
        }else{
               el.style.paddingTop = '0px';
        }
    }else{
    	var statusBar = api.require('statusBar');
		//同步返回结果：
		var statusBarHeight  = statusBar.getStatusBarHeight();
        if(parseFloat(api.systemVersion)>=4.4 && parseFloat(api.systemVersion)<=5.0){
            el.style.paddingTop = statusBarHeight+'px';
            el.style.backgroundColor = '#fec107';
        }else if(parseFloat(api.systemVersion)>5.0){
    		el.style.paddingTop = statusBarHeight+'px';
            el.style.backgroundColor = '#fec107';
        }else{
            el.style.paddingTop = '0px';
        }
    }
    api.setStatusBarStyle({
	    style: 'light',
	    color:'transparent'
	});
}


function sideOpenMyCoupons(el){
	$api.openWin({
        name:'coupon-list',
        url: 'widget://html/account/my-coupon-list.html',
        animation:{
            type:"movein",
            subType:"from_right"
        },
        reload:true
    });
}

//获取15分钟优惠券
var setTimeStam;
var thMinuteCouponsFlag = true;
function thMinuteCoupons(){
	if(getCId()){
		if(!thMinuteCouponsFlag) return;
		thMinuteCouponsFlag = false;
		$.mamall_request('coupon.findbycustomer',{customerId:getCId(),H5_API_REQUEST_CACHE_SET:2}, function(r) {
			if (r.ErrorCode == '9999'){
				clearInterval(setTimeStam);
				var couponsData = r.Data.coupon;
				if(couponsData.length > 0 ){
					var timeStampArr = [];
					for( var i = 0; i < couponsData.length; i++){
						timeStampArr.push(couponsData[i].leftTimestamp);
					}
					var minTimeStamp = Math.min.apply(Math, timeStampArr);
					$api.setStorage('minThMinuteCouponTimeStamp',minTimeStamp);
					setTimeStam = setInterval(function(){
						$api.setStorage('minThMinuteCouponTimeStamp',minTimeStamp--);
					},1000)
				}else{
					$api.rmStorage('minThMinuteCouponTimeStamp');
				}
				api.sendEvent({
					name: 'thMinutCouponsStatus'
				});
			}
			thMinuteCouponsFlag = true;
		},undefined,api);
	}
}
var sideCouponsTime = 0;
var shakeTime = 0;
function sideCouponsStatus(){
	clearInterval(sideCouponsTime);
	clearInterval(shakeTime);
	var minThMinuteCouponTimeStamp = $api.getStorage('minThMinuteCouponTimeStamp');
	formatSeconds(minThMinuteCouponTimeStamp);
	if($('.button-menu .icon0-coupon-side').length>0){
		$('.button-menu .icon0-coupon-side').remove();
	}
	if(minThMinuteCouponTimeStamp>0 || $api.getStorage('couponAlertTimeStatus')){
		var _html = '<li class="ourmallfont icon0-coupon-side shake-rotate" tapmode onclick="sideOpenMyCoupons()" id="sideCoupon"><i class="ourmallfont icon0-coupon-side"></i><span>Expired<span></span></span></li>'
		$('.button-menu').prepend(_html);
		if($('.icon0-coupon-side').length>0){
			shakeTime = setInterval(function(){
				$('.icon0-coupon-side:not(.full)').addClass('active');
				setTimeout(function(){
					$('.icon0-coupon-side').removeClass('active');
				},800)
			},5000)
		}
	}else{
		$('.button-menu .icon0-coupon-side').remove();
	}
	sideCouponsTime = setInterval(function(){
		var _val = $('.button-menu .icon0-coupon-side').attr('data-seconds');
		$('.button-menu .icon0-coupon-side>span>span').text(formatSeconds(_val));
		_val--;
		$('.button-menu .icon0-coupon-side').attr('data-seconds',_val);
		if(_val <= 0 ){
			clearInterval(sideCouponsTime);
			clearInterval(shakeTime);
			$('.button-menu .icon0-coupon-side').remove();
			sideCouponsStatus();
		}
	},1000)

}
function formatSeconds(value) {
	var second = parseInt(value);
	var minute = 0;

	if( second > 60){
		minute = parseInt(second/60);
		second = parseInt(second%60);
		result = (minute < 10 ? '0' + minute : minute) + ':' + (second<10 ? '0' + second : second)
	}else{
		result = '00:' + (second<10 ? '0' + second : second)
	}
	return result;
}
function formatSecondsHMS(value,type){
	var second = parseInt(value),
		minute = 0,
		hour = 0;
	hour = Math.floor(second/3600);
	minute = Math.floor(Math.floor(second%3600)/60);
	second = Math.floor(second%60);
	if(hour < 10)  hour = '0' + hour.toString();
	if(minute < 10)  minute = '0' + minute.toString();
	if(second < 10)  second = '0' + second.toString();
	switch(type){
		case 'h':
			return hour
			break;
		case 'm':
			return minute
			break;
		case 's':
			return second
			break;
		default:
		return hour + ' : ' + minute + ' : ' + second;
	}

}

function loadProperties() {
	var nowLanguage = localStorage.getItem('DEFAULT_APP_LANGUAGE') ? localStorage.getItem('DEFAULT_APP_LANGUAGE') : 'en';

	var resourcePath = document.location.pathname;
	var findString = '';
	var apicloudId = localStorage.getItem('APICLOUD_APP_ID');
	if(resourcePath.indexOf(apicloudId) >= 0) {
		findString = apicloudId + '/';
	} else {
		findString = 'widget/';
	}
	var startPos = resourcePath.indexOf(findString);
	resourcePath = resourcePath.substring(startPos);
	var arrPath = resourcePath.replace(findString, '').split('/');
    var relativePath=document.location.pathname.indexOf('/main.html') !== -1 || document.location.pathname.indexOf('/index.html') !== -1?'./i18n/':'../../i18n/';
	try {
		jQuery.i18n.properties({
	        name : 'strings',
	        path : relativePath,
	        mode : 'map',
	        language : nowLanguage,
	        callback : function() {
	            $('[i18n-tag]').each(function() {
					$(this).text(jQuery.i18n.prop($(this).text()));
                });
	        }
	    });} catch(e) {}
}
$(function(){
	loadProperties();
});

function getCurrencyCache() {
	var lastCurrencyCheckTimestamp = $api.getStorage('LAST_CURRENCY_CHECK_TIMESTAMP');
	lastCurrencyCheckTimestamp = typeof(lastCurrencyCheckTimestamp) == 'undefined' ? 0 : parseFloat(lastCurrencyCheckTimestamp);
	var nowTimestamp = Date.parse(new Date()) / 1000;
	var currencyCacheObject = $api.getStorage('LAST_CURRENCY_CACHE');
	currencyCacheObject = typeof(currencyCacheObject) == 'object' ? currencyCacheObject : {};
	if(parseInt(currencyCacheObject.EUR) != 1 || parseFloat(lastCurrencyCheckTimestamp) + 86400 < nowTimestamp) {
		//最后缓存货币对应表超过1天，接口重新获取新的数据
		$api.setStorage('LAST_CURRENCY_CHECK_TIMESTAMP', nowTimestamp);
		$.mamall_request('other.currencyrate',{source:'EUR'}, function(r) {
			if (r.ErrorCode == '9999'){
				//r.Data.CurrencyRate.EUR  货币兑换比例
				//r.Data.CurrencySign  货币符号
				currencyCacheObject = r.Data.CurrencyRate.EUR;
				currencySymbol = r.Data.CurrencySign;
				$api.setStorage('LAST_CURRENCY_CACHE', currencyCacheObject);
				$api.setStorage('LAST_CURRENCY_SYMBOL_CACHE', currencySymbol);
				return currencyCacheObject;
			}
		},undefined,api);
	} else {
		return currencyCacheObject;
	}
}
function getDisplayCurrency(displayType,forceCurrency) {
	if(forceCurrency) {
		var displayCurrency = forceCurrency;
	}else{
		var displayCurrency = $api.getStorage('APP_DISPLAY_CURRENCY') ? $api.getStorage('APP_DISPLAY_CURRENCY') : 'EUR';
	}
	var currencyCacheObject = getCurrencyCache();
	var currencyRate = 0;
	eval('if(typeof(currencyCacheObject) == "object" && currencyCacheObject.' + displayCurrency + ') { currencyRate = parseFloat(currencyCacheObject.' + displayCurrency + '); }');
	if(isNaN(currencyRate) || currencyRate <= 0) {
		displayCurrency = 'EUR';
	}
	if(displayType == 'symbol') {
		var currencySymbolCache = $api.getStorage('LAST_CURRENCY_SYMBOL_CACHE');
		currencySymbolCache = typeof(currencySymbolCache) == 'object' ? currencySymbolCache : {};
		eval('var symbol = currencySymbolCache.' + displayCurrency + ';');
		if(symbol) {
			return symbol;
		} else {
			return displayCurrency;
		}
	} else if(displayType == 'code') {
		return displayCurrency;
	}
	return displayCurrency;
}
function getDisplayCurrencyNumber(number) {
	//去除逗号格式化
	number = number.toString().replace(/\$|\,/g,'');
	var displayCurrencyCode = getDisplayCurrency('code');
	if(displayCurrencyCode != 'EUR') {
		var currencyCacheObject = getCurrencyCache();
		eval('var currencyRate = parseFloat(currencyCacheObject.' + displayCurrencyCode + ');');
		if(typeof(currencyCacheObject) == 'object' && currencyCacheObject.EUR && currencyRate > 0) {
			//本地有转换货币缓存
			eval('number = parseFloat(parseFloat(number) * parseFloat(currencyCacheObject.' + displayCurrencyCode + '));');
		}
	}
	return roundFixed(number);
}

//提示优惠券即将过期
function alertCouponExpire(_time){
	api.openFrame({
		name: 'pop-coupon-countdown-mask',
		url: 'widget://html/pop/pop-coupon-countdown-mask.html',
		bounces: false,
		bgColor: 'rgba(0,0,0,.7)',
		pageParam: {
			time:_time
		},
		vScrollBarEnabled: false,
		hScrollBarEnabled: false,
		reload: true
	});
}

function openLog(params){
	var DEVICE_IP_INFO = $api.getStorage('DEVICE_IP_INFO') ? JSON.parse($api.getStorage('DEVICE_IP_INFO')) : {};
	params.previewUrl=api.winName;
	params.appVersion=api.appVersion;
	if(params.pageParam){
		if(params.pageParam._pid) params.productId=params.pageParam._pid;
		if(params.pageParam._sid) params.sponsorShopId=params.pageParam._sid;
	}
	var detail = JSON.stringify(params);
	$.mamall_request('visit.log', {
		userTag:api.deviceId,
		userCode:getCId(),
		event:1,
		eventContent:params.name,
		source:params.name,
		ip:DEVICE_IP_INFO.cip,
		userAgent:api.systemType,
		screen:api.winWidth+','+api.winHeight,
		detail:detail
	}, function(r) {
		if (r.ErrorCode == '9999'){
		}
	},undefined,api);
}

//打开货币设置
function currencySetting(){
	$api.openWin({
		name: 'currency-setting',
		url: "widget://html/account/currency-setting.html",
        animation: {
            type: "movein",
            subType: "from_right"
        },
        reload: true
	})
}
function cardPayReturn(type,msg){
	switch(type){
		case 'success':
			payMask('success');
			setTimeout(function(){
				api.execScript({
				    name: 'main',
				    script: 'paySuccess()'
				});
			},3250);
		break;
		case 'failed':
			payMask('failed',msg.trim() ? msg.trim() : 'Failed');
		    setTimeout(function(){
				api.execScript({
				    name: 'main',
				    script: 'paySuccess()'
				});
			},2000);
		break;
	}
}
