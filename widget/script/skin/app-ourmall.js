var mallSubCategory = '{"ErrorCode":"9999","Message":"SUCCESS","Data":[{"id":9998, "name":"JUST FOR YOU"},{"id":9997, "name":"Giveaway"},{"id":1,"name":"Women’s Clothing","child":["Down Coats","Parkas","Dresses","Blouses","Shirts","Jumpsuits","Blazers","Hoodies","Sweatshirts","Basic Jackets","Trench","Cardigans","Pullovers","Shorts","Jeans","Pants","Skirts","Bras","Sunglasses","Belts","Scarves","Hats"]},{"id": 11,"name":"Health & Beauty","child":["Humain Hair","Hair Weaves","Eye","Face","Lip","Makeup tools","Health Care","Facial Care","Sun Care","Slimming Creams","Essetial Oils","Neil Polish","False Nails","Hair Scissors","Straightening Irons","Hair Dryers","Hair Rollers","Hair Color"]},{"id": 8,"name":"Bags & Shoes","child":["Shoulder Bags","Top-Handle Bags","Crossbody Bags","Wallets","Backpacks","Clutches","Purses","Casual Shoes","Sandals","Pumps","Flats","Boots","Slippers"]},{"id": 6,"name":"Jewelry & Watches","child":["Necklaces","Pendants","Rings","Earrings","Bracelets","Jewelry sets","Brooches","Key Chains","Charms","Body Jewelry","Quartz Watches","Digital Watches","Dual Display Watches","Sports Watches"]},{"id":9999, "name":"Recently Viewed"}]}';
var mainMenu = [
	{ id:'AppMenuMall', name: 'Mall', icon: 'icon0-mall2'},
	{ id:'AppMenuGroup', name: 'Deal', icon: 'icon0-deal'},
	{ id:'AppMenuExplore', name: 'Explore', icon: 'icon0-explore'},
	{ id:'AppMenuCart', name: 'Cart', icon: 'icon0-cart'},
	{ id:'AppMenuAccount', name: 'Account', icon: 'icon0-account'}
]
