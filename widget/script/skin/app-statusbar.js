//沉浸式
function fixStatusBar(el){
    if(api.systemType=="ios"){
        if(parseInt(api.systemVersion,10)>=7){
               el.style.paddingTop = '20px';
               el.style.backgroundColor = '#fad902';
        }else{
               el.style.paddingTop = '0px';
        }
        api.setStatusBarStyle({
		    style: 'light',
		    color:'#ffffff'
		});
    }else{
    	var statusBar = api.require('statusBar');
		//同步返回结果：
		var statusBarHeight  = statusBar.getStatusBarHeight();
        if(parseFloat(api.systemVersion)>=4.4 && parseFloat(api.systemVersion)<=5.0){
            el.style.paddingTop = statusBarHeight+'px';
            el.style.backgroundColor = '#fad902';
        }else if(parseFloat(api.systemVersion)>5.0){
    		el.style.paddingTop = statusBarHeight+'px';
            el.style.backgroundColor = '#fad902';
        }else{
            el.style.paddingTop = '0px';
        }
    }
}

