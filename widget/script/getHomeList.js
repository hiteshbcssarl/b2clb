var getDealList = creatDealList = clearFilter = function(){};
ARCommon = function(){
	var page = 1;
	var totalPage = "";
	var catId = api.frameName.replace(/[^0-9]/ig,"");
	var postUrl = "";
	var loadFlag = true;
	var categoryIds = 'categoryIds';
	var settledwords;
	var cacheData = null;
	var retryTimes = 0;
	if(catId == 9888){
		return;
	}
	switch(api.frameName){
		case (api.frameName.match(/homeGroup/) || {}).input:
		case (api.frameName.match(/mallGroup/) || {}).input:
			var wrapUlDom = $("#wrap .mall-pro-list");//商品 容器
			if(wrapUlDom.length==0){
				$("#wrap").html('<ul class="mall-pro-list"></ul>');
				wrapUlDom = $("#wrap .mall-pro-list");
			}
			postUrl = 'product.productlist';
			cacheData = getLocalPageData('MALL_GROUP_' + catId);
			break;
		case (api.frameName.match(/dealGroup/) || {}).input:
			var wrapUlDom = $("#wrap .mall-pro-list");//商品 容器
			if(wrapUlDom.length==0){
				$("#wrap").html('<ul class="mall-pro-list"></ul>');
				wrapUlDom = $("#wrap .mall-pro-list");
			}
			postUrl = 'product.seckilllist';
			break;
		default:
			break;
	}
	function shareExpire(){
		$("[data-expiretime]").each(function(){
			var expiretime = $(this).attr("[data-expiretime]");
			if(expiretime < Date.parse(new Date()) / 1000){
				$(this).find('.share-badge-warp').hide();
			}
		});
	}
	
	nui.pullrefresh(function(){
		if($('.pulldownload').is(':visible') || !loadFlag){
			return false;
		}
		page = 1;
		cacheData = null;
		if((api.frameName.match(/dealGroup/) || {}).input){
			getDealList();
		}else{
			removeLocalPageData('MALL_GROUP_' + catId);
			if(catId == '9000' || catId == '9001' || catId == '9002' ) getList();
			if (catId == 9999) {
				getRecentlyList();
			} else if (catId == 9998) {
				getLastestList();
			} else{
				getSettledwords();
			}
		}
		
	},'#f6f6f6')
	
	nui.pullAppend(function(){
		if($('.pulldownload').is(':visible') || !loadFlag){
			return false;
		}
		if((api.frameName.match(/dealGroup/) || {}).input){
			return;
		}else{
			if(catId == 9999){
				if(page >= totalPage ) {
					$('.pulldownload').hide();
					nui.toast($.i18n.prop("No more data"));
					return;
				}
				getRecentlyList("append");
			} else if(catId == 9998){
				if(page >= totalPage ) {
					$('.pulldownload').hide();
					nui.toast($.i18n.prop("No more data"));
					return;
				}
				getLastestList("append");
			} else{
				if(page >= totalPage ) {
					$('.pulldownload').hide();
					nui.toast($.i18n.prop("No more data"));
					return;
				}
				getList("append");
			}
		}
		
	});
	
	getList = function(type){
		if(!loadFlag){
			return false;
		}
		loadFlag = false;
		$('.pulldownload').show();
		var apiOption = {
			categoryIds:catId,
			page:type == "append" ? page + 1 : page,
			rowsPerPage:10
		};
		if(postUrl == 'product.productlist'){
			var tmpKeywords = ''; 
			if($api.getStorage('MallProductCategoryFilter')){
				var tmpObj = $api.getStorage('MallProductCategoryFilter') ? JSON.parse($api.getStorage('MallProductCategoryFilter')) : {};
				if(typeof(tmpObj) == 'object' && typeof(tmpObj[catId]) == 'object') {
					tmpKeywords = tmpObj[catId].nameLike;				
					if(tmpObj[catId].minPrice){
						apiOption.bonusFrom = tmpObj[catId].minPrice;
					}
					if(tmpObj[catId].maxPrice){
						apiOption.bonusTo = tmpObj[catId].maxPrice;
					}
					if(tmpObj[catId].sortBy){
						apiOption.sort = tmpObj[catId].sortBy;
					}
					if(typeof tmpObj[catId].specificsQueryCond == 'object'){
						apiOption.specificsQueryCond = '';
						for( var i = 0; i<tmpObj[catId].specificsQueryCond.length; i++){
							if(tmpObj[catId].specificsQueryCond[i] == 'All') continue;
							apiOption.specificsQueryCond += tmpObj[catId].specificsQueryCond[i] + ','
						}
						apiOption.specificsQueryCond = apiOption.specificsQueryCond.substring(0,apiOption.specificsQueryCond.length-1);
					}
				}
			}
			if(typeof(tmpObj) == 'object' && typeof(tmpObj[catId]) == 'object' && (tmpObj[catId].nameLike || apiOption.specificsQueryCond)){
				apiOption.categorytwo=tmpObj[catId].nameLike;
			} else if(page <= 2 && catId) {
				//第一、二页且有历史访问
				var RVproArr = JSON.parse(localStorage.getItem('RV-Key'));
				if(RVproArr) {
					var arrStar = 10 * ((type == "append" ? page + 1 : page)-1);
					var arrEnd = arrStar + 10;
					if(arrStar + 10 > RVproArr.length){
						arrEnd = RVproArr.length;
					}
					var thisArr = RVproArr.slice(arrStar,arrEnd);
					apiOption.recentlyViewed = RVArrFormat(thisArr);
				}
			} else {
				apiOption.settledwords=settledwords;
			}
			apiOption.categoryId=catId;
		}
		if(!apiOption.nameLike) delete apiOption.nameLike;
		if(! settledwords) { delete apiOption.settledwords; }
		
		if(apiOption.categoryIds == '9000' || apiOption.categoryIds == '9001' || apiOption.categoryIds == '9002'){
			switch(apiOption.categoryIds){
				case '9000':
					apiOption.isPopular = 1;
					break;
				case '9001':
					apiOption.isRecommended = 1;
					break;
				case '9002':
					apiOption.isSale = 1;
					break;	
			}
			delete apiOption.categoryIds;
			delete apiOption.categoryId;
		}
		$.mamall_request(postUrl,apiOption, function(r) {
			loadFlag = true;
			if('9999' == r.ErrorCode) {
				$('.pulldownload').hide();
				retryTimes = 0;
				var listJson = r.Data;
				if(listJson) {
					if(listJson.Pagination)
						totalPage = listJson.Pagination.totalpage;
					if(type == "append"){
						$(wrapUlDom).append(baidu.template("listTemp",listJson));
						page += 1;
					}else{
						if(postUrl == 'product.productlist' && listJson.marketList.length == 0){
							var tmpObj = $api.getStorage('MallProductCategoryFilter') ? JSON.parse($api.getStorage('MallProductCategoryFilter')) : {};
							if(tmpObj[catId] != void 0 && (tmpObj[catId].minPrice || tmpObj[catId].maxPrice || typeof tmpObj[catId].specificsQueryCond == 'object')){
								$('.no-filter-results').remove();
								var specificsTXT = '';
								if(typeof tmpObj[catId].specificsQueryCondTXT == 'object'){
									for( var i = 0; i<tmpObj[catId].specificsQueryCondTXT.length; i++){
										if(tmpObj[catId].specificsQueryCond[i] == 'All') continue;
										specificsTXT += tmpObj[catId].specificsQueryCondTXT[i] + ','
									}
									specificsTXT = specificsTXT.substring(0,specificsTXT.length-1);
								}
								$(wrapUlDom).after(baidu.template('alertFilterResults',{
										data:{
											categorytwo: tmpObj[catId].nameLikeTXT,
											specificsQueryCond: specificsTXT,
											bonusFrom: apiOption.bonusFrom,
											bonusTo: apiOption.bonusTo,
										}
									}
								));
							}else{
								$(wrapUlDom).html(baidu.template("listTemp",{marketList:'NoData'}));
							}
						}else{
							$(wrapUlDom).html(baidu.template("listTemp",listJson));
						}
					}
				}
				if(postUrl == 'product.productlist') {
					if(listJson.marketList.length >0){
						saveLocalPageData('MALL_GROUP_' + catId, $("#wrap").html(), page, totalPage);
					}else{
						removeLocalPageData('MALL_GROUP_' + catId);
					}	
				}
				imgLoad();
			}else if(r=='timeout' && retryTimes < 2){
				retryTimes ++;
				getList(type);
			}
			shareExpire();
		},undefined,api);
	}
	getRecentlyList = function(type){
		if(!loadFlag){
			return false;
		}
		loadFlag = false;
		$('.pulldownload').show();
		
		var RVproArr = JSON.parse(localStorage.getItem('RV-Key'));
		
		if(!RVproArr){
			$(wrapUlDom).html(baidu.template("listTemp",{marketList:'NoData'}));
			api.refreshHeaderLoadDone();
			$('.pulldownload').hide();
			loadFlag = true;
			return
		}

		var arrEnd = RVproArr.length-((type == "append" ? page + 1 : page)-1)*10;
		var arrStar = arrEnd-10
		if(arrStar<0){
			arrStar = 0;
		}
		
		var thisArr = RVproArr.slice(arrStar,arrEnd);
		var apiOption = {
			recentlyViewed: RVArrFormat(thisArr),
		};
		$.mamall_request('product.customerproducts', apiOption, function(r) {
			loadFlag = true;
			if('9999' == r.ErrorCode) {
				$('.pulldownload').hide();
				retryTimes = 0;
				var listJson = r.Data;
				if(listJson) {
					totalPage = Math.ceil(RVproArr.length/10);
					if(type == "append"){
						page ++;
					} else {
						$(wrapUlDom).html('');	
					}
					$(wrapUlDom).append(baidu.template("listTemp",listJson));
				}
				imgLoad();
				saveLocalPageData('MALL_GROUP_9999', $("#wrap").html(), page, totalPage);
			}else if(r=='timeout' && retryTimes < 2){
				retryTimes ++;
				getRecentlyList(type);
			}
			shareExpire();
		},undefined,api);
	}
	getLastestList = function(type){
		if(!loadFlag){
			return false;
		}
		loadFlag = false;
		$('.pulldownload').show();
		var RVproArr = JSON.parse(localStorage.getItem('RV-Key'));
		var apiOption = {page:type == "append" ? page + 1 : page};
		if(RVproArr){
			apiOption['recentlyViewed'] = RVArrFormat(RVproArr);
		}
		$.mamall_request('product.latestproducts', apiOption, function(r) {
			loadFlag = true;
			if('9999' == r.ErrorCode) {
				$('.pulldownload').hide();
				retryTimes = 0;
				var listJson = r.Data;
				if(listJson) {
					totalPage = listJson.Pagination.totalPage;
					if(type == "append"){
						page ++;
					}else{
						$(wrapUlDom).html('');
					}
					$(wrapUlDom).append(baidu.template("listTemp",listJson));
				}
				saveLocalPageData('MALL_GROUP_9998', $("#wrap").html(), page, totalPage);
				imgLoad();
			}else if(r=='timeout' && retryTimes < 2){
				retryTimes ++;
				getLastestList(type);
			}
			shareExpire();
		},undefined,api);
	}

	getTime = function(str){
		var _second = parseInt(str);
		var second = _second % 60 +"";
		var minute = (_second - second) / 60 +"";
		minute = minute.length == 1?"0"+minute:minute;
		second = second.length == 1?"0"+second:second;
		return minute + ":" + second;
	}
	
	function getSettledwords() {
		$.mamall_request('product.generatewords',{categoryId:catId}, function(r) {
			if('9999' == r.ErrorCode) {
				settledwords = r.Data.settledwords;	
			}
			getList();
		},undefined,api);
	}
	//
	getDealList = function(type){
		if(!loadFlag){
			return false;
		}
		loadFlag = false;
		$('.pulldownload').show();
		var dealListCache = $api.getStorage('DealList_Storage');
		if( typeof dealListCache == 'object' && Date.parse(new Date()) / 1000 - dealListCache.time < 60){
			loadFlag = true;
			api.refreshHeaderLoadDone();
			creatDealList(dealListCache.data);
			$('.pulldownload').hide();
		}else{
			$.mamall_request(postUrl, {			
				isSkillProduct:1
			}, function(r) {
				loadFlag = true;
				var nowTimestamp = Date.parse(new Date()) / 1000;
				if('9999' == r.ErrorCode) {
					$('.pulldownload').hide();
					var data = r.Data;
					$api.setStorage('DealList_Storage',{data: data,time: nowTimestamp});
					creatDealList(data);
					$('.pulldownload').hide();
				}
			},undefined,api);
		}
		
	}
	creatDealList = function(data){
		switch(catId){
			case '9000':
				//Previous Deals
				if(data.previous.length == 0){
					$(wrapUlDom).html(baidu.template("dealListTemp",{marketList:'NoData'}));
				}else{
					$(wrapUlDom).html(baidu.template("dealListTemp",{marketList:data.previous}));
				}
			break;	
			case '9001':
				//OnSale Now
				if(data.onSale.length == 0){
					$(wrapUlDom).html(baidu.template("dealListTemp",{marketList:'NoData'}));
				}else{
					$(wrapUlDom).html(baidu.template("dealListTemp",{marketList:data.onSale}));
				}
			break;
			case '9002':
				//Upcoming Deals
				if(data.upComing.length == 0){
					$(wrapUlDom).html(baidu.template("dealListTemp",{marketList:'NoData'}));
				}else{
					$(wrapUlDom).html(baidu.template("dealListTemp",{marketList:data.upComing}));
				}
			break;
			default:
			break;
		}
		setTimeout(function(){
			imgLoad();
		},500)
		
	};
	//是否是deal
	if((api.frameName.match(/dealGroup/) || {}).input){
		getDealList();
	}else{
		if(cacheData) {
			var nowTimestamp = Date.parse(new Date());
			nowTimestamp = nowTimestamp / 1000;
			if((nowTimestamp - cacheData.timestamp <= 43200)) {
				loadFlag = true;
				totalPage = cacheData.totalPage;
				page = cacheData.page;
				$('.pulldownload').hide();
			} else {
				api.refreshHeaderLoading();
			}
		} else {
			if(catId == 9999){
				getRecentlyList();
			} else if(catId == 9998) {
				getLastestList();
			}else if(catId == 9997) {
				getGiveAwayList();
			}else if ('product.productlist' == postUrl){
				var filterString = $api.getStorage('MallProductCategoryFilter');
				getSettledwords(filterString);
			} else {
				getList();
			}
		}
	}
	clearFilter = function(){
		var tmpObj = JSON.parse($api.getStorage('MallProductCategoryFilter'));
		if(tmpObj[catId].nameLike ||tmpObj[catId].minPrice || tmpObj[catId].maxPrice || typeof tmpObj[catId].specificsQueryCond == 'object'){
			tmpObj[catId].nameLike = '';
			tmpObj[catId].nameLikeTXT = '';
			tmpObj[catId].minPrice = '';
			tmpObj[catId].maxPrice = '';
			tmpObj[catId].specificsQueryCond = '';
			tmpObj[catId].specificsQueryCondTXT = '';
		}
		$api.setStorage('MallProductCategoryFilter',JSON.stringify(tmpObj));
		removeLocalPageData('MALL_GROUP_' + catId) //删除页面缓存
		api.execScript({
		    name: 'main',
			script: 'openFrameReload($("#mall ul>li[data-categoryId='+catId+']").index())'
		});	
		api.execScript({
			name:'main',
			script:'filterBadgeActive();'
		});
	}
	
	
	function RVArrFormat(arr){
		arr = JSON.stringify(arr);
		arr = arr.substr(1);
		arr = arr.substr(0,arr.length-1);
		arr = arr.replace(/},{/g,",");
		return arr;
	}
}

