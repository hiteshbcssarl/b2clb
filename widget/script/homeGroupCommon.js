function AROpenFun() {
	var proIdArr;
	openProDetail = function(type,_pid,_tpid,_sid,shareEarn,_kid) {
		switch($api.getStorage('activeMainMenu')){
			case 'mall':
			//沉浸式状态栏
			if(api.systemType=="ios"){
	            if(parseInt(api.systemVersion,10)>=7){
	               api.addEventListener({
					    name:'viewappear'
					}, function(ret, err){
						setTimeout(function(){
							api.setStatusBarStyle({
							    style: 'light',
							    color:'transparent'
							});
						},200);
					});
	           }
	    	}
			break;
		}

		if(!!_tpid && !!_sid){
			var proId = {};
			proId[_tpid+'_'+_sid] = 1;
			if(!localStorage.getItem('RV-Key')){
				localStorage.setItem('RV-Key','[]');
			}
			var proIdArr = JSON.parse(localStorage.getItem('RV-Key'));
			if(!Object.prototype.toString.call(proIdArr) === '[object Array]'){
				proIdArr = new Array;
			}
			for( var i=0; proIdArr.length>i;i++ ){

				for(var obj in proIdArr[i]){
					if(obj == _tpid+'_'+_sid){
						oldProId = proIdArr[i];
						proId[_tpid+'_'+_sid] = proIdArr[i][_tpid+'_'+_sid] +1;
						proIdArr.splice(i, 1);
						break;
					}
				}
			}
			proIdArr.push(proId);
			//proIdArr = getArray(proIdArr);
			if(proIdArr.length>=50){
				proIdArr.shift();
			}
			//存本地
			localStorage.setItem('RV-Key',JSON.stringify(proIdArr));
			//存接口
			var localUinfo = JSON.parse(localStorage.getItem('OURMALL_USERINFO'));
			var customerId;
			if(!isEmptyObject(localUinfo)) customerId = localUinfo.customerId;
			if(customerId){
				var crvApiOption = new Object;
				crvApiOption.customerId = customerId;
				crvApiOption.plus = _tpid+'_'+_sid;
				crvApiOption.productIdJson = JSON.stringify(proIdArr);
				$.mamall_request('customer.recentlyViewed', crvApiOption, function(){},undefined,api);
			}
		}
		$api.openWin({
	        name: 'pro-detail-platform',
	        url: "widget://html/product/pro-detail-platform.html",
	        pageParam: {_pid:_tpid,_sid:_sid,_kid:_kid},//这里是要打开的网址
	        animation: {
	            type: "movein",
	            subType: "from_right"
	        },
	        reload: true
	    });
	}
	openReviewList = function(reviewInfo) {
		$api.openWin({
	        name: 'pro-detail-review',
	        url: "widget://html/product/pro-detail-review.html",
	        pageParam: {reviewInfo:reviewInfo},//这里是要打开的网址
	        animation: {
	            type: "movein",
	            subType: "from_right"
	        },
	        reload: true
	    });
	}

	openVlgReviewList = function(reviewInfo) {
		$api.openWin({
	        name: 'VloggerFeedbackList',
	        url: "widget://html/product/pro-detail-vlg-review.html",
	        pageParam: {reviewInfo:reviewInfo},//这里是要打开的网址
	        animation: {
	            type: "movein",
	            subType: "from_right"
	        },
	        reload: true
	    });
	}
}
function getArray(a) {
 var hash = {},
     len = a.length,
     result = [];

 for (var i = 0; i < len; i++){
     if (!hash[a[i]]){
         hash[a[i]] = true;
         result.push(a[i]);
     }
 }
 return result;
}

function openBuyerProtection() {
	$api.openWin({
		name: "buyer-protection",
		url: "widget://html/account/buyer-protection.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
