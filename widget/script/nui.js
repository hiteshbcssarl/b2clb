/*
 * Javascript Document
 * Creat by Nelson 2016/11/18
 * 
 * Website:https://segmentfault.com/u/nelson2016
 * 
 * QQ:616859570
 * Email:Nelson_Lee@outlook.com,Nelson2016@aliyun.com
 * */
function htmlspecialchars(str){            
	str = str.replace(/&/g, '&amp;');  
	str = str.replace(/</g, '&lt;');  
	str = str.replace(/>/g, '&gt;');  
	str = str.replace(/"/g, '&quot;');  
	str = str.replace(/'/g, '&#039;');  
	return str;  
}
var umengAnalytics;
function onAnalyticStart(thisPageName) {
	api.addEventListener({
        name:'viewappear'
 }, function(ret, err){
        // 此处建议用异步接口
        umengAnalytics.onPageStart({
            pageName: thisPageName,
        },function(ret,err){
             if(ret.status){
                console.log('开始统计“' + thisPageName + '”页面');
             }else{
                console.log(ret.code+' '+ret.msg);
             }
        });
        api.execScript({
	    	name: 'root',
	    	script: ' thMinuteCoupons()'
	    })
    });
    // iOS手势关闭页面时结束自定义页面统计
    api.addEventListener({
        name:'viewdisappear'
    },function(ret,err){
        onAnalyticEnd(thisPageName);
    });
}
function onAnalyticEnd(thisPageName) {
	// 此处建议用同步接口
    var ret = umengAnalytics.onPageEndSync({
        pageName: thisPageName
    });
    if(ret.status){
        console.log('结束统计“' + thisPageName + '”页面');
    }else{
        console.log(ret.code+' '+ret.msg);
    }
}


apiready = function() {
	umengAnalytics = api.require('umengAnalytics');
	umengAnalytics.init({
        debugMode:false,
    },function(ret, err){
        if(ret.status){
            onAnalyticStart(api.winName + '_' + JSON.stringify(api.pageParam));
        }
    });
    if((! localStorage.getItem('OURMALL_UMENGID')) || (localStorage.getItem('OURMALL_UMENGID') == 'undefined')) {
    	umengAnalytics.testDeviceID(function(ret, err){
	        if((!err) && ret){
	        	var umengDevice = JSON.parse(ret);
	        	if(umengDevice.oid) {
	        		localStorage.setItem('OURMALL_UMENGID', umengDevice.oid);
	        	} else if(umengDevice.device_id) {
	        		localStorage.setItem('OURMALL_UMENGID', umengDevice.device_id);
	        	}
	        }
	    });
    }

	var backBtn = document.querySelector(".nui-header-back");
	var scrollTopBtn = document.getElementById("scroll-top");
	var statusBar = document.querySelector(".header_status");
	if(scrollTopBtn){
		window.onscroll = function(e){
			var ct = document.documentElement.scrollTop || document.body.scrollTop;
			if(ct <= 10){
				scrollTopBtn.style.display = "none";
			}else{
				scrollTopBtn.style.display = "block";
			}
		}
	};
	
	if(statusBar){
		window.onscroll = function(e){
			var ct = document.documentElement.scrollTop || document.body.scrollTop;
			if(ct <= 10){
				statusBar.style.backgroundColor = "rgba(0,0,0,.05)";
			}else{
				if(api.systemType=="ios"){
		            if(parseInt(api.systemVersion,10)>=7){
		                  statusBar.style.backgroundColor = '#fec107';
		                   
		            }else{
		                  statusBar.style.backgroundColor = "transparent";
		                  
		            }
			    }else{
			    	if(parseFloat(api.systemVersion)>=4.4 && parseFloat(api.systemVersion)<=5.0){
	                        statusBar.style.backgroundColor  = '#fec107';
	                        
		                }else if(parseFloat(api.systemVersion)>5.0){
		                		statusBar.style.backgroundColor = '#fec107';
		                        
		                }else{
		                	statusBar.style.backgroundColor = "transparent";
		                }
			    };
			}
		}
	};
	if(scrollTopBtn && statusBar){
		window.onscroll = function(e){
			var ct = document.documentElement.scrollTop || document.body.scrollTop;
			if(ct <= 10){
				scrollTopBtn.style.display = "none";
				statusBar.style.backgroundColor = "rgba(0,0,0,.05)";
			}else{
				scrollTopBtn.style.display = "block";
				if(api.systemType=="ios"){
		            if(parseInt(api.systemVersion,10)>=7){
		                  statusBar.style.backgroundColor = '#fec107';
		                   
		            }else{
		                  statusBar.style.backgroundColor = "transparent";
		                  
		            }
			    }else{
			    	if(parseFloat(api.systemVersion)>=4.4 && parseFloat(api.systemVersion)<=5.0){
	                        statusBar.style.backgroundColor  = '#fec107';
	                        
		                }else if(parseFloat(api.systemVersion)>5.0){
		                		statusBar.style.backgroundColor = '#fec107';
		                        
		                }else{
		                	statusBar.style.backgroundColor = "transparent";
		                }
			    };
			
			}
		}
	};

	if(backBtn) {
		backBtn.onclick = function() {
			onAnalyticEnd(api.winName);
			api.sendEvent({
			    name: 'shareComplete'
			});			
			api.closeWin({});
		}
	}
	api.addEventListener({
	    name:'keyback'
	},function(ret,err){
	    //operation
		onAnalyticEnd(api.winName);
		api.closeWin({});
	});
	var headerStatusBar = $api.byId("nui-headers");
	if(headerStatusBar) {
		$api.fixIos7Bar(headerStatusBar);
	}

	if((typeof ARRequire).toLowerCase() == "function") {
		ARRequire();
	}
	if((typeof AROpenFun).toLowerCase() == "function") {
		AROpenFun();
	}
	if((typeof AR).toLowerCase() == "function") {
		AR();
	}
	if((typeof ARCommon).toLowerCase() == "function") {
		ARCommon();
	}
	
	api.addEventListener({
		name: 'thMinutCouponsStatus'
	},function(ret, err){
		sideCouponsStatus();		    
		
	});
	api.addEventListener({
	    name: 'CustomerServiceStatus'
	}, function(ret, err) {
		if(ret.value.status == true){
			$('#toChatMessage').addClass('active');
			localStorage.setItem('CustomerServiceUnreadStatus', 'true');
		}else{
			localStorage.removeItem('CustomerServiceUnreadStatus');
			$('#toChatMessage').removeClass('active');
		} 
	});

	var touchElement = {};
	window.addEventListener('touchstart',function(e){
		touchElement.pageX = 0;
		touchElement.pageY = 0;
		window.addEventListener('touchmove',function(event){
			touchElement.pageX = event.targetTouches[0].pageX;
			touchElement.pageY = event.targetTouches[0].pageY;
			
		})
	});

	var forbiddenLogPageArr = ['card-pay'];
	window.addEventListener("touchend",function(e){
		//不在forbiddenLogPageArr数字中，可以记录日志
		if(!in_array(api.winName, forbiddenLogPageArr) && touchElement.pageX==0 && touchElement.pageY==0){
			setTimeout(function(){
				var DEVICE_IP_INFO = $api.getStorage('DEVICE_IP_INFO') ? JSON.parse($api.getStorage('DEVICE_IP_INFO')) : {};
				if(e.target){
					var attr = e.target.attributes;
					var detail = {
						nodeName:e.target.nodeName
					}			
					if(e.target.innerText){
						if(attr.title){
							var eventContent = attr.title.nodeValue+' '+e.target.innerText;
							detail.title = attr.title.nodeValue;
						}else{
							var eventContent = e.target.innerText;
						}
						detail.innerText = e.target.innerText;
					}else if(attr.title){
						var eventContent = attr.title.nodeValue; 
						detail.title = attr.title.nodeValue;
					}else if(attr.alt){
						var eventContent = attr.alt.nodeValue; 
						detail.alt = attr.alt.nodeValue; 
					}
					if(eventContent){
						if(attr.class) detail.class=attr.class.nodeValue;
						if(attr.name) detail.name=attr.name.nodeValue;
						if(attr.id) detail.id=attr.id.nodeValue;
						if(attr.onclick) detail.onclick=attr.onclick.nodeValue;
						detail.appVersion=api.appVersion;
						detail = addLogParams(detail);
						detail = JSON.stringify(detail);
						$.mamall_request('visit.log', {
							userTag:api.deviceId,
							userCode:getCId(),
							event:2,
							eventContent:eventContent,
							source:api.winName,
							ip:DEVICE_IP_INFO.cip,
							userAgent:api.systemType,
							screen:api.winWidth+','+api.winHeight,
							detail:detail
						}, function(r) {
							if (r.ErrorCode == '9999'){	
							}
						},undefined,api);	
					}
				}
			},300);
		}
	});	

	$("input,textarea").on('blur',function(e){
		//不在forbiddenLogPageArr数字中，可以记录日志
		if(!in_array(api.winName, forbiddenLogPageArr) && e.target){
			var DEVICE_IP_INFO = $api.getStorage('DEVICE_IP_INFO') ? JSON.parse($api.getStorage('DEVICE_IP_INFO')) : {};
			var attr = e.target.attributes;
			if(e.target.value){

				var detail = {
					innerText:e.target.value,
				}
				if(attr.class) detail.class=attr.class.nodeValue;
				if(attr.name) detail.name=attr.name.nodeValue;
				if(attr.id) detail.id=attr.id.nodeValue;
				if(attr.onclick) detail.onclick=attr.onclick.nodeValue;
				detail.appVersion=api.appVersion;
				detail = addLogParams(detail);
				detail = JSON.stringify(detail);
				$.mamall_request('visit.log', {
					userTag:api.deviceId,
					userCode:getCId(),
					event:3,
					eventContent:e.target.value,
					source:api.winName,
					ip:DEVICE_IP_INFO.cip,
					userAgent:api.systemType,
					screen:api.winWidth+','+api.winHeight,
					detail:detail
				}, function(r) {
					if (r.ErrorCode == '9999'){	
					}
				},undefined,api);	
			}
		}		
	});

	function addLogParams(detail){
        if(typeof logParams !='undefined') {
            $.each(logParams,function(m,n){
                detail[m]=n;
            });
        }   
        return detail;     
    }	
	
}

/**
 * MUI核心JS
 * @type _L4.$|Function
 */
var nui = (function(document, undefined) {

	var idSelectorRE = /^#([\w-]+)$/;

	var $ = function(selector, context) {
		context = context || document;
		if(!selector) {
			return main();
		} else {
			try {
				selector = selector.trim();
				if(idSelectorRE.test(selector)) {
					var found = document.getElementById(RegExp.$1);
					return main(found ? [found] : []);
				}
				return main($.QSAll(selector, context), selector);
			} catch(e) {}
		}
		return main()
	};

	var main = function(dom, selector) {
		dom = dom || [];
		Object.setPrototypeOf(dom, $.fn);
		dom.selector = selector || '';
		return dom;
	};

	/**
	 * mui querySelectorAll
	 * @param {type} selector
	 * @param {type} context
	 * @returns {Array}
	 */
	$.QSAll = function(selector, context) {
		context = context || document;
		return $.slice.call(classSelectorRE.test(selector) ? context.getElementsByClassName(RegExp.$1) : tagSelectorRE.test(selector) ? context.getElementsByTagName(selector) : context.querySelectorAll(selector));
	};
	/**
	 * $.fn
	 */
	$.fn = {
		each: function(callback) {
			[].every.call(this, function(el, idx) {
				return callback.call(el, idx, el) !== false;
			});
			return this;
		}
	};
	/**
	 * each
	 * @param {type} elements
	 * @param {type} callback
	 * @returns {_L8.$}
	 */
	$.each = function(elements, callback, hasOwnProperty) {
		if(!elements) {
			return this;
		}
		if(typeof elements.length === 'number') {
			[].every.call(elements, function(el, idx) {
				return callback.call(el, idx, el) !== false;
			});
		} else {
			for(var key in elements) {
				if(hasOwnProperty) {
					if(elements.hasOwnProperty(key)) {
						if(callback.call(elements[key], key, elements[key]) === false) return elements;
					}
				} else {
					if(callback.call(elements[key], key, elements[key]) === false) return elements;
				}
			}
		}
		return this;
	};

	/**
	 * @description Get Add & Remove class
	 * @param {String} classStr
	 */
	$.toggleClass = function(dom, classStr, type) {
		if(classStr == undefined) {
			return dom.classList;
		} else if(classStr !== undefined && type !== undefined) {
			dom.classList[type](classStr);
		} else if(classStr !== undefined) {
			return dom.classList.contains(classStr);
		}
	}
	/**
	 * @description Get dom css
	 * @param {Document} dom Document Ele
	 * @param {String} style
	 */
	$.getStyle = function(dom, style) {
		if(dom.currentStyle) {
			return dom.currentStyle[style];
		} else if(window.getComputedStyle) {
			return document.defaultView.getComputedStyle(dom, null)[style];
		}
		return null;
	}

	$.toast = function(msg) {
		api.toast({
			msg: msg,
			duration: 2000,
			location: 'bottom'
		});
	}
	/*Append some elements to a other elements*/
	$.appendHTML = function(dom,html) {
	    var divTemp = document.createElement("div"), nodes = null, fragment = document.createDocumentFragment();
	    divTemp.innerHTML = html;
	    nodes = divTemp.childNodes;
	    for (var i=0, length=nodes.length; i<length; i+=1) {
	       fragment.appendChild(nodes[i].cloneNode(true));
	    }
	    dom.appendChild(fragment);
	    nodes = null;
	    fragment = null;
	};
	/*Append some elements to a other elements*/
	$.insertBeforeHTML = function(dom,html) {
	    var divTemp = document.createElement("div"), nodes = null, fragment = document.createDocumentFragment();
	    divTemp.innerHTML = html;
	    nodes = divTemp.childNodes;
	    for (var i=0, length=nodes.length; i<length; i+=1) {
	       fragment.appendChild(nodes[i].cloneNode(true));
	    }
	    dom.insertBefore(fragment,dom.children[0]);
	    nodes = null;
	    fragment = null;
	};

	/**
	 * @description Pullrefresh
	 * @param {Function} callback
	 * @param {String} bgColor
	 */
	$.pullrefresh = function(callback, bgColor ,loadGif) {
		bgColor = bgColor ? bgColor : '#FFFFFF';
		loadGif = loadGif ? loadGif : 'loading_more.gif';
		api.setRefreshHeaderInfo({
			visible : true,
			loadingImg : 'widget://image/'+loadGif,
			bgColor : bgColor,
			textColor : '#999999',
			textDown : 'Pull down to refresh',
			textUp : 'Release to refresh',
			showTime : true
		}, function(ret, err) {
			$api.setStorage('LIST_PULL_REFRESH_FLAG', 'true');
			if(callback){
				callback();
			}
		});
	}
	/**
	 * @description Pullappend
	 * @param {Function} callback
	 * @param {String} bgColor
	 */
	$.pullAppend = function(callback,dis) {
		api.addEventListener({
			name : 'scrolltobottom',
			extra : {
				threshold : dis?dis:100
			}
		}, function(ret, err) {
			$api.rmStorage('LIST_PULL_REFRESH_FLAG');
			if(callback){
				callback();
			}
		});
	}
	/**
	 * @description initExitApp
	 */
	$.initExitApp = function() {
		api.addEventListener({
			name : 'keyback'
		}, function(ret, err) {
			api.toast({
				msg : 'Press back button to EXIT',
				duration : 2000,
				location : 'bottom'
			});
			api.addEventListener({
				name : 'keyback'
			}, function(ret, err) {
				api.closeWidget({
					id : api.appId,
					retData : {
						name : 'closeWidget'
					},
					silent : true
				});
			});
			setTimeout(function() {
				exitApp();
			}, 2000)
		});
	}
	/**
	 * @description Open window
	 * @param {Object} windowobj
	 */
	$.openwindow = function(windowobj) {
		var name = windowobj._url.substring(windowobj._url.lastIndexOf("/") + 1, windowobj._url.length);
		$api.openWin({
			name: name + (windowobj._namePrefix ? windowobj._namePrefix : ""),
			url: windowobj._url + ".html",
			pageParam: windowobj.para ? windowobj.para : {},
			animation: {
				type: windowobj._aniType ? windowobj._aniType : "movein",
				subType: windowobj._aniSubType ? windowobj._aniSubType : "from_right",
				duration: windowobj._aniDur ? windowobj._aniDur : 300,
			},
			bgColor: "rgba(0,0,0,.8)",
			reload: true,
			customRefreshHeader: windowobj.refreshStr ? windowobj.refreshStr : "",
			bounces: windowobj.bounces == true ? windowobj.bounces : false,
			slidBackEnabled: windowobj.slideBack == false ? windowobj.slideBack : true,
			delay: 0,
		});
	}
	/**
	 * @description Open frame
	 * @param {Object} windowobj
	 */
	$.openFrame = function(windowobj) {
		var defaultY = $api.byId("nui-headers") ? $api.byId("nui-headers").offsetHeight : 45;
		var name = windowobj._url.substring(windowobj._url.lastIndexOf("/") + 1, windowobj._url.length);
		api.openFrame({
			name: name,
			url: windowobj._url + ".html",
			rect: {
				x: windowobj._x === undefined ? 0 : windowobj._x,
				y: windowobj._y === undefined ? defaultY : windowobj._y,
				w: windowobj._w === undefined ? "auto" : windowobj._w,
				h: windowobj._h === undefined ? "auto" : windowobj._h,
			},
			bgColor: "rgba(0,0,0,0)",
			pageParam: windowobj.para ? windowobj.para : {},
			bounces: windowobj.bounces == undefined ? true : windowobj.bounces,
			reload: true,
			vScrollBarEnabled: true,
			hScrollBarEnabled: true
		});
	}
	
	/**
	 * @description Ajax get function
	 * @param {Object} obj
	 */
	$.ajaxGet = function(obj) {
		api.ajax({
			url: obj._url,
			method: 'get',
			data: {
				values: obj._data,
			},
			dataType: "text",
			charset: "utf-8",
			timeout: 5,
		}, function(ret, err) {
			var result = JSON.parse(ret);
			if(ret) {
				obj._suc(result.Status, result);
				api.hideProgress();
				api.refreshHeaderLoadDone();
			} else {
				api.hideProgress();
				api.refreshHeaderLoadDone();
				if(obj._err) {
					obj._err();
				}
			}
		});
	}
	/**
	 * @description Ajax post function
	 * @param {Object} obj
	 */
	$.ajaxPost = function(obj) {
		api.ajax({
			url: obj._url,
			method: 'post',
			data: {
				values: obj._data,
			},
			dataType: "text",
			charset: "utf-8",
			timeout: 5,
		}, function(ret, err) {
			var result = JSON.parse(ret);
			if(ret) {
				obj._suc(result.Status, result);
				api.hideProgress();
				api.refreshHeaderLoadDone();
			} else {
				api.hideProgress();
				api.refreshHeaderLoadDone();
				if(obj._err) {
					obj._err();
				}
			}
		});
	}
	
	$.fixStatusBar = function(dom,check) {
	  var strDM = api.systemType;
	  if (strDM == 'ios') {
	      var strSV = api.systemVersion;
	      var numSV = parseInt(strSV,10);
	      var fullScreen = api.fullScreen;
	      var iOS7StatusBarAppearance = api.iOS7StatusBarAppearance;
	      if (numSV >= 7 && !fullScreen && iOS7StatusBarAppearance) {
	      	if(!check){
	      		dom.style.paddingTop = '20px';
	      	}
	          return true
	      }
	  }
	  return false;
	}
	
	$.isIOS = function(){
		return api.systemType == 'ios';
	}
	
	return $;

})(document);

/**
 * @description Init numberBox
 */
nui.initNumberBox = function(callback,callbackNormal,minToast,maxToast) {
	var numberBoxAllDom = document.querySelectorAll(".nui-number-box");
	this.each(numberBoxAllDom, function(i, item) {
		var reduceBtn = item.children[0];
		var addBtn = item.children[2];
		var numInput = item.children[1].children[0];
		var min = parseFloat(item.getAttribute("data-minNum"));
		var max = item.getAttribute("data-maxNum") == "max" ? "max" : parseFloat(item.getAttribute("data-maxNum"));
		var newValue = numInput.value;
		reduceBtn.onclick = function() {
			var currentValyue = parseFloat(numInput.value);
			if(currentValyue - 1 < min) {
				if(minToast){
					minToast();
				}
				return
			};
			numInput.value = currentValyue - 1;
			newValue = currentValyue - 1;
			if(callback) {
				callback(item, currentValyue, newValue)
			};
		};
		addBtn.onclick = function() {
			var currentValyue = parseFloat(numInput.value);
			max = item.getAttribute("data-maxNum") == "max" ? "max" : parseFloat(item.getAttribute("data-maxNum"));
			if(currentValyue + 1 > max && max != "max") {
				if(maxToast){
					maxToast();
				}
				return
			};
			numInput.value = currentValyue + 1;
			newValue = currentValyue + 1;
			if(callback) {
				callback(item, currentValyue, newValue)
			};
		};
		numInput.oninput = function() {
			this.value = parseFloat(this.value.replace('/[^d]+/g', ''));
			max = item.getAttribute("data-maxNum") == "max" ? "max" : parseFloat(item.getAttribute("data-maxNum"));
			if(this.value > max && max != "max") {
				this.value = max;
				if(maxToast){
					maxToast();
				}
			}
			if(this.value < min) {
				if(minToast){
					minToast();
				}
				this.value = min;
			}
			if(callbackNormal) {
				callbackNormal(item, this.value, this.value)
			};
		}
		numInput.onpropertychange = function() {
			this.value = this.value.replace('/[^d]+/g', '');
			if(this.value > max && max != "max") {
				this.value = max;
			}
			if(this.value < min) {
				this.value = min;
			}
			if(callbackNormal) {
				callbackNormal(item, this.value, this.value)
			};
		}
		numInput.onblur = function() {
			if(callback) {
				callback(item, newValue, this.value)
			};
		}
	})
}

nui.fn.foldList = function(callback) {
	var ITEM = ".nui-fold-list-item";
	var CONTAINER = ".nui-fold-list-container";
	var _dom = this[0];
	var _items = _dom.querySelectorAll(ITEM);
	nui.each(_items, function(i, item) {
		item.onclick = function(e) {
			e.stopPropagation();
			if(!item.querySelector(CONTAINER)) {
				callback();
				return;
			}
			var curItemContainer = this.parentNode;
			var siblingItems = curItemContainer.querySelectorAll(".active");
			nui.each(siblingItems, function(i, item) {
				item.classList.remove("active");
			})
			this.classList.add("active");
		}
	})
}

/**
 * @description Banner slider
 */
var banenrSlider = (function(doc, exportName) {
	var _console = window.console && window.console.log;
	var CONTAINER = ".nui-slider-container";
	var ITEM = ".nui-slider-item";
	var PAGINATION_CLASS = "pagination";
	var PAGINATION = "." + PAGINATION_CLASS;
	var PAGINATION_CONTAINER_CLASS = "pagination-container"
	var PAGINATION_CONTAINER = "." + PAGINATION_CONTAINER_CLASS;
	var PAGINATION_ITEM_CLASS = "pagination-item";
	var PAGINATION_ITEM = "." + PAGINATION_ITEM_CLASS;
	var PREFIX = ['webkit', 'moz', 'ms', ''];
	var HALF_CRITICAL = 100;

	var $ = function(dom, options) {
		if((typeof dom).toLowerCase() != "object") {
			if(_console) {
				console.error('nelsonVS()所传入的元素无法执行此操作')
			}
			return;
		};
		if(!('ontouchstart' in window)) {
			if(_console) {
				console.error('您的浏览器不支持touch事件')
			}
			return;
		}
		this.defaultOptions = {
			width: document.body.clientWidth,
			height: document.body.clientWidth * 2 / 3,
			damping: 2,
			speed: 300,
			showPagination: true,
			paginationPos: "right"
		}
		this.options = this.extend(this.defaultOptions, options);
		this.init(dom);

		return this;
	}
	$.prototype = {
		slideInterval: "",
		isMoving: false,
		init: function(dom) {
			this.dom = dom.querySelector(CONTAINER);
			this.items = dom.querySelectorAll(ITEM);
			this.index = 0;
			this.count = this.items.length;

			var firstNode, secondNode;

			if(this.count == 2) {
				this._count = 2;
				this.dom.appendChild(this.items[0].cloneNode(true));
				this.dom.appendChild(this.items[1].cloneNode(true));
			}else if(this.count == 1){
				this._count = 1;
				var nodeTemp = this.items[0].cloneNode(true);
				this.dom.appendChild(nodeTemp);
				this.dom.appendChild(nodeTemp.cloneNode(true));
			}

			this.items = dom.querySelectorAll(ITEM);
			this.count = this.items.length;
			this.itemWidth = this.options.width;
			this.itemHeight = this.options.height;
			HALF_CRITICAL = this.options.width / 3;

			this.initPagination();

			this.format();
			return this;
		},
		format: function() {
			this.dom.style.width = this.itemWidth + "px";
			this.dom.style.height = this.itemHeight + "px";
			for(var i = 0; i < this.count; i++) {
				for(var j = 0; j < PREFIX.length; j++) {
					this.items[i] && (this.items[i].style[PREFIX[j] + "Transform"] = "translateX(" + this.itemWidth * i + "px)");
				}
			}

			this.bindEvt();
		},
		bindEvt: function() {
			var startX, startY, endX, endY, touchstartStemp, isScrolling, distanceX, distanceY,
				that = this;
			this.dom.addEventListener("touchstart", function(e) {
				var touchObj = e.changedTouches[0];
				touchstartStemp = new Date().valueOf();
				startX = parseInt(touchObj.pageX);
				startY = parseInt(touchObj.pageY);
				distanceX = 0;
				distanceY = 0;
			})
			this.dom.addEventListener("touchmove", function(e) {
				var touchObj = e.targetTouches[0]
				endX = parseInt(touchObj.pageX);
				endY = parseInt(touchObj.pageY);
				distanceX = endX - startX;
				distanceY = endY - startY;

				var direction = that.getDirection(distanceX, distanceY);

				if(direction == "left" || direction == "right") {
					e.preventDefault();
					that.move(distanceX / that.options.damping, 0, 'touchmove');
				}
			})
			this.dom.addEventListener("touchend", function(e) {
				distanceX = distanceX == undefined ? 0 : distanceX;
				distanceY = distanceY == undefined ? 0 : distanceY;

				var direction = that.getDirection(distanceX, distanceY);
				var duration = new Date().valueOf() - touchstartStemp;
				var canScroll = (duration < 300 && Math.abs(distanceX) < 30 && Math.abs(distanceX) >= 10) || (Math.abs(distanceX) > HALF_CRITICAL);
				var isTap = duration < 150 && Math.abs(distanceX) < 10;
				if(isTap){
					that.onTapCalblack(that.index);
					that.move(distanceX, that.options.speed, 'reset');
				}else{
					if(canScroll) {
						that.move(distanceX, that.options.speed, direction == 'left' ? "nxt" : "pre");
					} else {
						that.move(distanceX, that.options.speed, 'reset');
					}
				}

			})
		},
		getDirection: function(x, y) {
			if(x === y) {
				return '';
			}
			if(Math.abs(x) >= Math.abs(y)) {
				return x > 0 ? 'right' : 'left';
			} else {
				return y > 0 ? 'down' : 'up';
			}
		},
		move: function(d, s, t) {
			var preIndex = this.index == 0 ? this.count - 1 : this.index - 1;
			var nxtIndex = this.index == this.count - 1 ? 0 : this.index + 1;
			var preDistance, nxtDistance, curDistance;

			switch(t) {
				case 'touchmove':
					preDistance = -this.options.width + d;
					curDistance = d;
					nxtDistance = this.options.width + d;
					break;
				case 'pre':
					preDistance = 0;
					curDistance = this.options.width;
					nxtDistance = this.options.width * 2;
					break;
				case 'nxt':
					preDistance = -this.options.width * 2;
					curDistance = -this.options.width;
					nxtDistance = 0;
					break;
				case 'reset':
					preDistance = -this.options.width;
					curDistance = 0;
					nxtDistance = this.options.width;
					break;
			}

			for(var i = 0; i < PREFIX.length; i++) {
				this.items[this.index].style[PREFIX[i] + 'TransitionDuration'] = s + "ms";
				this.items[this.index].style[PREFIX[i] + 'Transform'] = "translateX(" + curDistance + "px)";
				this.items[preIndex].style[PREFIX[i] + 'TransitionDuration'] = s + "ms";
				this.items[preIndex].style[PREFIX[i] + 'Transform'] = "translateX(" + preDistance + "px)";
				this.items[nxtIndex].style[PREFIX[i] + 'TransitionDuration'] = s + "ms";
				this.items[nxtIndex].style[PREFIX[i] + 'Transform'] = "translateX(" + nxtDistance + "px)";
			}
			if(t == "nxt") {
				this.index = nxtIndex;
				this.updatePagination(nxtIndex);
			} else if(t == 'pre') {
				this.index = preIndex;
				this.updatePagination(preIndex);
			}
		},
		initPagination: function() {
			var _count = this._count ? this._count : this.count;

			var paginationDom = document.createElement("div");
			paginationDom.className = PAGINATION_CLASS;

			var paginationContainerDom = document.createElement("ul");
			paginationContainerDom.className = PAGINATION_CONTAINER_CLASS + " " + this.options.paginationPos;
			paginationContainerDom.style.width = 16 * _count - 8 + "px";

			for(var i = 0; i < _count; i++) {
				var _liNode = document.createElement("li");
				_liNode.className = PAGINATION_ITEM_CLASS + (i == 0 ? " active" : "");
				paginationContainerDom.appendChild(_liNode);
			}

			paginationDom.appendChild(paginationContainerDom);
			this.dom.parentNode.appendChild(paginationDom);

			this.paginationContainer = document.querySelector(PAGINATION_CONTAINER);
			this.paginationItems = document.querySelectorAll(PAGINATION_ITEM);

			paginationDom = undefined;
			paginationContainerDom = undefined;
		},
		updatePagination: function(_index) {
			if(this._count == 2 && _index == 2) {
				_index = 0;
			} else if(this._count == 2 && _index == 3) {
				_index = 1
			}else if(this._count == 1 && (_index == 0 || _index == 1 || _index == 2)){
				_index = 0;
			}
			this.paginationContainer.querySelector(".active").classList.remove("active");
			this.paginationItems[_index].classList.add("active");
		},
		extend: function(a, b) {
			for(var key in b) {
				if(b.hasOwnProperty(key)) {
					a[key] = b[key];
				}
			}
			return a;
		},
		onTap: function(callback) {
			if((typeof callback).toLowerCase() == 'function')
				this.onTapCalblack = callback;
		},
		onTapCalblack: function() {},
		getCurrentIndex:function(){
			var _index = this.index;
			if(this._count == 2 && this.index == 2) {
				_index = 0;
			} else if(this._count == 2 && this.index == 3) {
				_index = 1
			}else if(this._count == 1 && (this.index == 0 || this.index == 1 || this.index == 2)){
				_index = 0;
			}
			return _index;
		}
	}

	function NVS(dom, options) {
		return new $(dom, options);
	}

	window[exportName] = NVS;

})(document, 'nelsonBannerSlider');