//Account页面相关js方法
//打开用户账户设置
function openAccountSetting(){
	$api.openWin({
		name: "account-settings",
		url: "widget://html/account/account-settings.html",
		animation: {
			type: "movein",
            subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//打开用户账户设置 名字
function openAccountSettingName(){
	$api.openWin({
		name: "account-settings-name",
		url: "widget://html/account/account-settings-name.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//打开用户账户设置 邮件
function openAccountSettingEmail(){
	$api.openWin({
		name: "account-settings-email",
		url: "widget://html/account/account-settings-email.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//打开用户账户设置 手机
function openAccountSettingMobile(){
	$api.openWin({
		name: "account-settings-mobile",
		url: "widget://html/account/account-settings-mobile.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//打开用户账户设置 密码
function openAccountSettingPassword(){
	$api.openWin({
		name: "account-settings-password",
		url: "widget://html/account/account-settings-password.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}

//忘记密码窗口
function openForgetPsd(){
	$api.openWin({
        name: "forget-psd",
        url: "widget://html/account/forget-psd.html",
        animation: {
            type: "movein",
            subType: "from_right"
        },
        reload: true
    });
}
//登录注册窗口
function openSignIn(){
	$api.openWin({
        name: "sign-in",
        url: "widget://html/account/sign-in.html",
        animation: {
            type: "movein",
            subType: "from_right"
        },
        reload: true
    });
}
function openRegister(){
	$api.openWin({
        name: "sign-up",
        url: "widget://html/account/sign-up.html",
        animation: {
            type: "movein",
            subType: "from_right"
        },
        reload: true
    });
}
//my-favorites
function openMyfavorite() {
	$api.openWin({
		name: "myfavorite",
		url: "widget://html/account/my-favorites.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//about-ourmall
function openAbout() {
	$api.openWin({
		name: "aboutourmall",
		url: "widget://html/account/about-ourmall.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//privacy
function openPrivacy() {
	$api.openWin({
		name: "privacy",
		url: "widget://html/account/privacy.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//openConditions
function openConditions() {
	$api.openWin({
		name: "conditions",
		url: "widget://html/account/conditions.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//openfaq
function openFAQ() {
	$api.openWin({
		name: "faq",
		url: "widget://html/account/faq.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//feedback
function openFeedback() {
	$api.openWin({
		name: "feedback",
		url: "widget://html/account/feedback.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		bgColor:"#fff",
		reload: true
	});
}
//setting
function openSetting() {
	$api.openWin({
		name: "AccountSetting",
		url: "widget://html/account/setting.html",
		animation: {
			type: "movein",
			subType: "from_right"
		},
		reload: true
	});
}
//打开订单列表
function openMyOrder(status) {
	if(!status) status = 0;
	$api.openWin({
		name:'my-orders',
		url : 'widget://html/account/my-orders.html',
		animation: {
			type: "movein",
			subType: "from_right"
		},
		pageParam:{
			status:status
		},
		reload: true
	});
};
//打开地址列表
function openAddressList() {
    $api.openWin({
        name:'address-list',
        url: 'widget://html/account/address-list.html',
        pageParam: { fromAccount:1 },
        animation:{
            type:"movein",
            subType:"from_right"
        },
        reload:true
    });
}
function countrySetting(country) {
    var appSupportCountry = $api.getStorage('appSupportCountry');
    country = country ? country : '';
    var supportCountryKeyArr = [];
    var supportCountryValueArr = [];
    if(appSupportCountry && appSupportCountry.length > 0) {
        for(var i = 0; i < appSupportCountry.length; i ++) {
            supportCountryKeyArr[i] = appSupportCountry[i].key;
            supportCountryValueArr[i] = appSupportCountry[i].value;
        }
    }
    if(in_array(country, supportCountryKeyArr)) {
        api.confirm({
            title: $.i18n.prop("Change Country") + '?',
            msg: $.i18n.prop("ZoodMall App will be restart after changing the country."),
            buttons: [$.i18n.prop("CANCEL"), $.i18n.prop("OK")]
        }, function(ret, err) {
            var index = ret.buttonIndex;
            if(index == 2) {
                localStorage.setItem('APP_MARKET_CODE', country);
                api.showProgress({
                    style: 'default',
                    animationType: 'fade',
                    title: $.i18n.prop("Please wait..."),
                    text: '',
                });

                      page = 1;
                      cacheData = null;
                var  catId='9000';
                      if((api.frameName.match(/dealGroup/) || {}).input){
                          getDealList();
                      }else{
                          removeLocalPageData('MALL_GROUP_'+catId);
                	getList();
                      }
                setTimeout(function(){ api.rebootApp(); }, 1000);
            }
        });
    } else {
        var nowCountry = localStorage.getItem('APP_MARKET_CODE') ? localStorage.getItem('APP_MARKET_CODE') : 'IR';
        var buttonArray = [];
        var selectedStr = '';
        var selectedButtonIndex = 0;
        for(var i = 0; i < supportCountryKeyArr.length; i ++) {
            selectedStr = ' ';
            if(supportCountryKeyArr[i] == nowCountry) {
                selectedStr = '\u2713 ';
                selectedButtonIndex = i + 1;
            }
            buttonArray.push(selectedStr + supportCountryValueArr[i]);
        }

        api.actionSheet({
            title: $.i18n.prop("Change Country"),
            cancelTitle: $.i18n.prop("CANCEL"),
            buttons: buttonArray
        }, function(ret, err) {
            var index = ret.buttonIndex;
            if(index <= supportCountryKeyArr.length) {
                if(selectedButtonIndex != index) {
                    countrySetting(supportCountryKeyArr[index-1]);
                }
            }
        });
    }
}
function languageSetting(lang) {
	var appSupportLanguage = $api.getStorage('appSupportLanguage');
	lang = lang ? lang : '';
	var supportLanguageKeyArr = [];
	var supportLanguageValueArr = [];
	if(appSupportLanguage && appSupportLanguage.length > 0) {
		for(var i = 0; i < appSupportLanguage.length; i ++) {
			supportLanguageKeyArr[i] = appSupportLanguage[i].key;
			supportLanguageValueArr[i] = appSupportLanguage[i].value;
		}

	}
	if(in_array(lang, supportLanguageKeyArr)) {
		api.confirm({
		    title: $.i18n.prop("Change Language") + '?',
		    msg: $.i18n.prop("ZoodMall App will be restart after changing the language."),
		    buttons: [$.i18n.prop("CANCEL"), $.i18n.prop("OK")]
		}, function(ret, err) {
		    var index = ret.buttonIndex;
		    if(index == 2) {
		    	localStorage.setItem('DEFAULT_APP_LANGUAGE', lang);
		    	api.showProgress({
		            style: 'default',
		            animationType: 'fade',
		            title: $.i18n.prop("Please wait..."),
		            text: '',
		        });
		    	setTimeout(function(){ api.rebootApp(); }, 1000);
		    }
		});
	} else {
		var nowLanguage = localStorage.getItem('DEFAULT_APP_LANGUAGE') ? localStorage.getItem('DEFAULT_APP_LANGUAGE') : 'en';
		var buttonArray = [];
		var selectedStr = '';
		var selectedButtonIndex = 0;
		for(var i = 0; i < supportLanguageKeyArr.length; i ++) {
			selectedStr = ' ';
			if(supportLanguageKeyArr[i] == nowLanguage) {
				selectedStr = '\u2713 ';
				selectedButtonIndex = i + 1;
			}
			buttonArray.push(selectedStr + supportLanguageValueArr[i]);
		}

		api.actionSheet({
		    title: $.i18n.prop("Change Language"),
		    cancelTitle: $.i18n.prop("CANCEL"),
		    buttons: buttonArray
		}, function(ret, err) {
		    var index = ret.buttonIndex;
		    if(index <= supportLanguageKeyArr.length) {
		    	if(selectedButtonIndex != index) {
			    	languageSetting(supportLanguageKeyArr[index-1]);
			    }
		    }
		});
	}
}
//获取订单数量
function getMenuNumber(){
	if(getCId()) {
		var loadingId = '';
	    var UILoading = api.require('UILoading');
	    UILoading.flower({
	        center: {
	            x: api.winWidth / 2.0,
	            y: api.winHeight / 2.0
	        },
	        size: 40,
	        fixed: true
	    }, function(ret) {
	        loadingId = ret;
	    });

	    $.mamall_request('b2c.account.menunumber',{
	        OM_DEVICE_UUID:api.deviceId,
	        customerId:getCId()
	    }, function(r) {
	        if('9999' == r.ErrorCode) {
		    	var localUinfo = localStorage.getItem('OURMALL_USERINFO');
		        localUinfo = localUinfo ? JSON.parse(localUinfo) : {};
	            if(loadingId) {
	                UILoading.closeFlower(loadingId);
	            }
	            $('#couponNum').text(r.Data.Coupon ? r.Data.Coupon : 0)//优惠券数量
	            $('#cashNum').text(r.Data.Cash ? getDisplayCurrencyNumber(r.Data.Cash) : '0.00')//优惠券数量
	            //订单、未读消息、Feedback、任务数量
	            for(var name in r.Data) {
	                var thisObj = r.Data[name];
	                if(name == 'Order') {
	                    for(var i in thisObj){
	                        if(thisObj[i] > 0) {
	                            $('#spanCnt' + i).html(thisObj[i]).show();
	                        }
	                    }
	                } else {
	                    if(typeof(thisObj) == 'object' && thisObj != null && thisObj.cnt) {
	                    	if(name != 'Feedback') {
		                        $('#spanCnt' + name).html(thisObj.cnt);
	                    	}
		                    $('#spanCnt' + name).show()
	                    }
	                }
	            }
	        }
	    }, undefined, api);
	}
}
//打开my-application列表
function openApplication() {
	$api.openWin({
        name: 'my-application',
        url: "widget://html/account/my-application.html",
        animation: {
            type: "movein",
            subType: "from_right"
        },
        reload: true
    });
}
